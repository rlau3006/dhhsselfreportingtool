package com.dataagility.dbwriter;

import com.dataagility.dbwriter.etc.Constants;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.parser.AuditLogFileParser;
import com.dataagility.dbwriter.thread.DBWriterRunnable;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;

import static com.dataagility.dbwriter.etc.Constants.DEFAULT_MAIN_THREAD_SLEEP_TIME_IN_MILLISECONDS;
import static com.dataagility.dbwriter.etc.Constants.parserFullPackageNameWithPeriod;
import static java.lang.System.err;
import static java.lang.System.exit;

/**
 * author: roger lau 2019
 */
public class ProofOfConceptDBWriterMain {

    private static File configFile = new File("C:\\Users\\rlau\\code.area\\ExceptionReportingTool\\src\\main\\resources\\dbwriter.cfg");
    private static List<AuditLogFileParserThreadConfiguration> threadStartupConfigurationLines = new ArrayList<>();
    private static Map<String, Thread> logFileParserThreadList = new HashMap<>();

    public static void main(String[] args) throws IOException, DBWriterException {

        LineIterator lineIterator = IOUtils.lineIterator(new FileInputStream(configFile), null);

        int lineCount = 0;
        while (lineIterator.hasNext()) {

            lineCount++;
            try {

                String configLine = lineIterator.nextLine();
                if (StringUtils.isEmpty(configLine) || configLine.startsWith("--"))
                    continue;

                AuditLogFileParserThreadConfiguration threadConfigLine = new AuditLogFileParserThreadConfiguration(configLine, lineCount);
                threadStartupConfigurationLines.add(threadConfigLine);

            } catch (DBWriterException e) {
                e.printStackTrace();
            }
        }

        startUpAuditLogFileParserThreads();

        try {
            while (true) {
                Thread.sleep(DEFAULT_MAIN_THREAD_SLEEP_TIME_IN_MILLISECONDS);
                System.out.println("ProofOfConceptDBWriterMain thread awake");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void startUpAuditLogFileParserThreads() throws DBWriterException {

        //set up the thread factory using the supplied builder class
        BasicThreadFactory.Builder threadfactorybuilder = new BasicThreadFactory.Builder();
        threadfactorybuilder.uncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.err.println("uncaughtException method called...");
                e.printStackTrace();
                System.exit(1);
            }
        });
        threadfactorybuilder.daemon(false);
        BasicThreadFactory threadFactory = threadfactorybuilder.build();

        Iterator<AuditLogFileParserThreadConfiguration> threadConfigurationIterator = threadStartupConfigurationLines.iterator();

        while ( threadConfigurationIterator.hasNext() ) {

            AuditLogFileParserThreadConfiguration threadConfiguration = threadConfigurationIterator.next();

            //now create the thread to run the log file parser
            String threadName = threadConfiguration.getParser().getFilename();
            Thread currentThread = threadFactory.newThread(new DBWriterRunnable(threadName, threadConfiguration.getSleepTimeInMilliseconds(), threadConfiguration.getParser()));
            currentThread.setName(threadName);

            //now start the log file parser thread
            currentThread.start();

            logFileParserThreadList.put(threadName, currentThread);
        }
    }


    private static class AuditLogFileParserThreadConfiguration {
        int lineNumber;
        String parserClassnameStr;
        String sleepTimeStr;
        String filePathStr;

        AuditLogFileParser parser;
        long sleepTimeInMilliseconds;

        public AuditLogFileParserThreadConfiguration(String dbwriterConfigLine, int lineNumber) throws DBWriterException {
            this.lineNumber = lineNumber;

            //path-to-audit-log-file,AuditLogFileParser class, sleep-period
            String[] configItemsArray = StringUtils.splitPreserveAllTokens(dbwriterConfigLine, ",");

            this.parserClassnameStr = configItemsArray[0];
            this.sleepTimeStr = configItemsArray[1];
            this.filePathStr = configItemsArray[2];

            //now put together the log file parser
            this.parser = buildLogFileParser(this.parserClassnameStr, lineNumber);
            this.parser.setLogFile(this.filePathStr);
            this.sleepTimeInMilliseconds = parseSleepTimeConfigString(this.sleepTimeStr);
        }

        private AuditLogFileParser buildLogFileParser(String parserTypeClassname, int lineNumber) {

            AuditLogFileParser parser = null;
            try {
                parserTypeClassname = parserFullPackageNameWithPeriod + parserTypeClassname;
                Class<?> clazz = Class.forName(parserTypeClassname);
                Constructor constructor = clazz.getConstructor();
                parser = (AuditLogFileParser) constructor.newInstance();

            } catch (ReflectiveOperationException e) {
                err.println("FATAL_ERROR: error found in config file: [" + configFile.getAbsolutePath() + "]");
                System.err.println("FATAL_ERROR: bad classname found at: [line " + lineNumber + "]");
                System.err.println("FATAL_ERROR: there was a problem instantiating an AuditLogFileParser using classname: [" + parserTypeClassname + "]");
                System.err.println("FATAL_ERROR: check that this is a valid classname and it exists on the classpath to this utility");
                exit(1);
            }

            return parser;
        }

        private long parseSleepTimeConfigString(String configString) {
            long sleepTimeInMilliseconds = Constants.DEFAULT_THREAD_SLEEP_TIME_IN_MILLISECONDS;
            try {
                sleepTimeInMilliseconds = Long.parseLong(configString);
            } catch (NumberFormatException e) {
                //do nothing - use the DEFAULT_THREAD_SLEEP_TIME_IN_MILLISECONDS
            }
            return sleepTimeInMilliseconds;
        }

        public void init() throws DBWriterException {

        }

        public AuditLogFileParser getParser() {
            return parser;
        }

        public long getSleepTimeInMilliseconds() {
            return sleepTimeInMilliseconds;
        }
    }
}
