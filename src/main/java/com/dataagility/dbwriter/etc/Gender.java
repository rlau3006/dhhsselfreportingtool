package com.dataagility.dbwriter.etc;

public enum Gender {
    MALE,
    FEMALE;
}
