package com.dataagility.dbwriter.etc;

/**
 * author: roger lau 2019
 */
public enum Acknowledgement {
    ACK,
    NACK,
    ERROR;
}
