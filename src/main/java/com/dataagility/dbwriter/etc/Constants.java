package com.dataagility.dbwriter.etc;

import org.apache.commons.validator.routines.RegexValidator;

/**
 * author: roger lau 2019
 */
public interface Constants {

    Character outputFileDelimiterChar = ',';
    //matches "201ymmddhh" or "201ymmddhhMM" or "201ymmddhhMMSS
    RegexValidator dateTimestampRegEx =
            new RegexValidator("([2][0][1][0-9][0,1][0-9][0-3][0-9][0-9]+|[2][0][1][0-9][0,1][0-9][0-3][0-9][0-2][0-9][0-9]+|[2][0][1][0-9][0,1][0-9][0-3][0-9][0-2][0-9][0-5][0-9][0-9]+)");
    RegexValidator segmentRegex = new RegexValidator("[A-Z][A-Z][A-Z0-9]\\|.+");

    String parserFullPackageNameWithPeriod = "com.dataagility.dbwriter.parser.";

    long DEFAULT_THREAD_SLEEP_TIME_IN_MILLISECONDS = 5 * 60 * 1000; //5 minutes

    long DEFAULT_MAIN_THREAD_SLEEP_TIME_IN_MILLISECONDS = 1 * 60 * 60 * 1000;  //1 hour

    boolean RUN_LOCAL_MACHINE = true;

    final int SYSTEM_EXIT_NORMAL_CODE_0 = 0;
    final int SYSTEM_EXIT_BAD_COMMAND_LINE_ARG_CODE_1 = 1;
    final int SYSTEM_EXIT_BAD_CONFIG_FILE_LINE_CODE_2 = 2;
    final int SYSTEM_EXIT_UNCAUGHT_EXCEPTION_CODE_3 = 3;
    final int SYSTEM_EXIT_INCORRECT_CLASSNAME_IN_CONFIG_LINE_CODE_4 = 4;
    final int SYSTEM_EXIT_CODE_5 = 5;
}
