package com.dataagility.dbwriter.etc;

public class DBWSequence {
    private static Object lockObject = new Object();
    private static int DBW_AUDIT_ID = 0;
    private static int DBW_EVENTS_ID = 0;
    private static int DBW_ARCHIVE_ID = 0;

    public static String getNextAuditId() {
        synchronized (lockObject) {
            return Integer.toString(DBW_AUDIT_ID++);
        }
    }

    public static String getNextEventId() {
        synchronized (lockObject) {
            return Integer.toString(DBW_EVENTS_ID++);
        }
    }

    public static String getNextArchiveId() {
        synchronized (lockObject) {
            return Integer.toString(DBW_ARCHIVE_ID++);
        }
    }
}
