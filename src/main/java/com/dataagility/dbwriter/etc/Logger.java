package com.dataagility.dbwriter.etc;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import static java.lang.System.err;
import static java.lang.System.out;

public class Logger {

    private static File logfile = null;
    private static boolean filelogging = false;

    boolean debug = false;

    public Logger(boolean filelogging) {
        this.filelogging = filelogging;
    }

    public Logger() {}

    public void initLogger(String logfilename) {
        try {
            logfile = new File(logfilename);
        } catch (Exception e) {
            filelogging = false;
        }
    }

    protected void write(String msg) {

        msg = msg + "\n";

        if(filelogging) {
            try {
                FileUtils.writeStringToFile(logfile, msg,(String)null, true);
            } catch (IOException e) {
                e.printStackTrace();
                err.println(msg);
            }
        } else {
            out.println(msg);
        }
    }

    public void writeToFileAndStdout(String msg) {
        out.println(msg);
        write(msg);
    }

    public void info(String msg) {
        write(msg);
    }

    public void error(String msg) {
        write(msg);
    }

    public void debug(String msg) {
        write(msg);
    }

    public boolean isDebugEnabled() {
        return debug;
    }
}