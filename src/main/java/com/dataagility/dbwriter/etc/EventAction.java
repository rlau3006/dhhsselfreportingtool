package com.dataagility.dbwriter.etc;

public enum EventAction {
    SENT,
    RECEIVED;
}
