package com.dataagility.dbwriter.etc;

import com.dataagility.dbwriter.DBWriter;
import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * author: roger lau 2019
 */
public class Utils {

    public static SimpleDateFormat datetimeWithMillisFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
    public static SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    public static SimpleDateFormat sentReceivedLineDatetimeFormat = new SimpleDateFormat("dd:MM:yyyy - HH:mm:ss:SSS");
    public static SimpleDateFormat logfileDateFormat = new SimpleDateFormat("yyyyMMdd");

    public final static String emptyString = "";

    public static String getSegmentByNameFromHL7Message(String segmentName, String hl7Message) {
        String[] segmentLines = StringUtils.split(hl7Message, '\r');
        if (segmentLines.length == 0)
            return emptyString;
        for (int i = 0; i < segmentLines.length; i++) {
            if (segmentLines[i].startsWith(segmentName))
                return segmentLines[i];
        }
        return emptyString;
    }

    public static String getFieldFromSegmentByNumber(int fieldNumber, String segmentStr) {
        String[] fieldsArray = StringUtils.splitPreserveAllTokens(segmentStr, '|');
        if ( segmentStr.startsWith("MSH"))
            fieldNumber--;
        if (fieldsArray.length < fieldNumber)
            return emptyString;
        else {
            String value = fieldsArray[fieldNumber];
            if ( "\"\"".matches(value))
                return emptyString;
            else
                return value;
        }
    }

    public static String getSubFieldFromFieldByNumber(int subfieldNumber, String fieldStr) {
        String[] subfieldsArray = StringUtils.splitPreserveAllTokens(fieldStr, '^');
        if (subfieldsArray.length < subfieldNumber)
            return emptyString;
        else {
            String value = subfieldsArray[subfieldNumber - 1];
            if ( "\"\"".matches(value))
                return emptyString;
            else
                return value;
        }
    }

    public static String getSubFieldFromSegmentByNumbers(int subfieldNumber, int fieldNumber, String segmentStr) {
        return getSubFieldFromFieldByNumber(subfieldNumber, getFieldFromSegmentByNumber(fieldNumber, segmentStr));
    }

    public static List<String> getRepeatSegmentsByName(String segmentName, String hl7Message) {
        List<String> repeatSegmentsList = new ArrayList<String>();
        String[] segmentLines = StringUtils.split(hl7Message, '\r');
        if (segmentLines.length == 0)
            return repeatSegmentsList;
        for (int i = 0; i < segmentLines.length; i++) {
            if (segmentLines[i].startsWith(segmentName))
                repeatSegmentsList.add(segmentLines[i]);
        }
        return repeatSegmentsList;
    }

    public static String getFieldFromRepeatFieldByNumbers(int repeatPositionNumber, String repeatFieldsStr) {
        if ( isEmpty(repeatFieldsStr))
            return emptyString;
        String[] repeatFieldsArray = StringUtils.splitPreserveAllTokens(repeatFieldsStr, "~");
        if ( repeatFieldsArray.length == 0 || repeatPositionNumber > repeatFieldsArray.length)
            return emptyString;
        return repeatFieldsArray[repeatPositionNumber-1];
    }

    public static boolean isEmpty(String testString) {
        boolean answer = StringUtils.isEmpty(testString);
        if ( !answer ) {
            if ( "\"\"".matches(testString.trim()))
                answer = true;
        }
        return answer;
    }

    public static boolean isNotEmpty(String testString) {
        return !isEmpty(testString);
    }

    public static String stripOffTimezone(String dateTimeStr) {
        return StringUtils.substringBefore(dateTimeStr, "+");
    }

    public static String findIdRootValue(String xmldoc) {
        String openingElementXml = "<id root=\"";
        int openingPos = xmldoc.indexOf(openingElementXml);
        int closingPos = xmldoc.indexOf('"', openingPos+openingElementXml.length());
        return StringUtils.substringAfter(xmldoc.substring(openingPos, closingPos), openingElementXml);
    }

    public static String findDocumentOIDValue(String xmldoc) {
        //<!-- Document UUID:69f623e3-1507-4a56-8a90-7b3fe79c73fb -->
        String openingElementXml = "<!-- Document UUID:";
        int openingPos = xmldoc.indexOf(openingElementXml);
        int closingPos = xmldoc.indexOf(" -->", openingPos+openingElementXml.length());
        return StringUtils.substringAfter(xmldoc.substring(openingPos, closingPos), openingElementXml);

    }

    public static String findDocumentSetIDValue(String xmldoc) {
        //<setId root="69f623e3-1507-4a56-8a90-7b3fe79c73fb" />
        String openingElementXml = "<setId root=\"";
        int openingPos = xmldoc.indexOf(openingElementXml);
        int closingPos = xmldoc.indexOf('"', openingPos+openingElementXml.length());
        return StringUtils.substringAfter(xmldoc.substring(openingPos, closingPos), openingElementXml);

    }

    public static String findXmlValueByElementName(String elementName, String xmldoc) {
        String openingElementXml = "<" + elementName + ">";
        String closingElementXml = "</" + elementName + ">";
        int openingPos = xmldoc.indexOf(openingElementXml);
        int closingPos = xmldoc.indexOf(closingElementXml);
        return StringUtils.substringAfter(xmldoc.substring(openingPos, closingPos), openingElementXml);
    }

    public static String getCurrentDateInLogFileFormat() {
        return logfileDateFormat.format(DBWriter.getCurrentDate());
    }
}
