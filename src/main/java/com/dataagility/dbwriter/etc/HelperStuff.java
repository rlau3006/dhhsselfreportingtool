package com.dataagility.dbwriter.etc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * author: roger lau 2019
 */
public class HelperStuff {

    /*
     HH24:MM:SS
     */
    private static DateFormat dateAndTimeLogFileFormat = new SimpleDateFormat("DD:MM:YYYY");
    public static Date stringToDate(String date, String time) throws ParseException {
//        return dateAndTimeLogFileFormat.parseFile(date + " " + time);
        return dateAndTimeLogFileFormat.parse(date);
    }

    public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
}
