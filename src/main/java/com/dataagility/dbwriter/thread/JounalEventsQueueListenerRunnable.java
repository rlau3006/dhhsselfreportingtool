package com.dataagility.dbwriter.thread;

import com.dataagility.dbwriter.dao.DAO;
import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.JournalEventsPair;

/**
 * author: roger lau 2019
 */
public class JounalEventsQueueListenerRunnable implements Runnable {

    private boolean up = true;
    private int sleepDurationMilliseconds = 1000;
    JournalEventThreadsafeDoubleEndedQueue threadsafeDoubleEndedQueue = null;
    DAO dbwriterDao = null;
    private Logger log = new Logger();

    public JounalEventsQueueListenerRunnable(JournalEventThreadsafeDoubleEndedQueue threadsafeDoubleEndedQueue, DAO dbwriterDao) {
        this.threadsafeDoubleEndedQueue = threadsafeDoubleEndedQueue;
        this.dbwriterDao = dbwriterDao;
    }

    @Override
    public void run() {

        while (up) {

            if (threadsafeDoubleEndedQueue.hasNext()) {

                JournalEventsPair eventsPair = threadsafeDoubleEndedQueue.next();

                try {

                    //todo build up a batch of writeJournalEvent requests to send in a batch to the DB
                    dbwriterDao.write(eventsPair);

                } catch (DBWriterException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    log.info("JounalEventsQueueListenerRunnable: thread about to sleep for 10seconds");
                    Thread.sleep(10000);   //todo put this in the config file
                    log.info("JounalEventsQueueListenerRunnable: sleep OVER");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void shutdown() {
        up = false;
    }

    public void setSleepDurationMilliseconds(String sleepDurationMilliseconds) {
        try {
            this.sleepDurationMilliseconds = Integer.parseInt(sleepDurationMilliseconds);
        } catch (NumberFormatException e) {
            //just use the default value for this.sleepDurationMilliseconds
        }
    }
}
