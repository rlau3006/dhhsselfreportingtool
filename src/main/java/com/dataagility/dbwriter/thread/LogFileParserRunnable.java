package com.dataagility.dbwriter.thread;

import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.exception.DBWriterEndOfFileException;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.parser.AuditLogFileParser;

/**
 * author: roger lau 2019
 */
public class LogFileParserRunnable implements Runnable {

    String threadname;
    long sleepInMilliSeconds;
    private AuditLogFileParser parser;
    private Logger log = new Logger();

    public LogFileParserRunnable(String threadname, long sleepTime, AuditLogFileParser parser) {
        this.threadname = threadname;
        this.sleepInMilliSeconds = sleepTime;
        this.parser = parser;
    }

    @Override
    public void run() {
        printConfig();
        log.writeToFileAndStdout("starting LogFileParserRunnable.run...");
        while (true) {

            try {

                parser.parseFile();

            } catch (DBWriterEndOfFileException eofe) {
                log.info(threadname + ": reached end of file");
                try {
                    log.info(threadname + ": thread about to sleep for (ms) " + sleepInMilliSeconds);
                    Thread.sleep(sleepInMilliSeconds);
                    log.info(threadname + ": sleeping over");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (DBWriterException dbwe) {
                dbwe.printStackTrace();
                //this is more serious and we should logwrite the error and exit this thread
                break;
            }
        }
    }

    private void logwrite(String msg) {
        log.info(threadname + ": " + msg);
    }

    private void printConfig() {
        log.writeToFileAndStdout("starting " + this.threadname + " set with " + this.sleepInMilliSeconds + " (ms)");
    }
}
