package com.dataagility.dbwriter.thread;

import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.model.JournalEventsPair;

import java.util.LinkedList;

/**
 * author: roger lau 2019
 */
public class JournalEventThreadsafeDoubleEndedQueue {

    LinkedList<JournalEventsPair> threadsafeJournalEventQueue = new LinkedList<>();
    private Logger log = new Logger();

    public void add(JournalEventsPair journalEventsPair) {
        synchronized (threadsafeJournalEventQueue) {
            log.info("adding JournalEventsPair to the threadsafe queue");
            threadsafeJournalEventQueue.add(journalEventsPair);
            log.info("size now:" + threadsafeJournalEventQueue.size());
        }
    }

    public boolean hasNext() {
        synchronized (threadsafeJournalEventQueue) {
            return threadsafeJournalEventQueue.size() > 0;
        }
    }

    public JournalEventsPair next() {
        synchronized (threadsafeJournalEventQueue) {
            return threadsafeJournalEventQueue.remove();
        }
    }
}
