package com.dataagility.dbwriter.thread;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import static com.dataagility.dbwriter.etc.Constants.SYSTEM_EXIT_UNCAUGHT_EXCEPTION_CODE_3;
import static java.lang.System.err;
import static java.lang.System.exit;

/**
 * author: roger lau 2019
 */
public class ThreadFactoryHelper {

    public static BasicThreadFactory singletonThreadFactory = null;

    static {
        //set up the thread factory using the supplied builder class
        BasicThreadFactory.Builder threadfactorybuilder = new BasicThreadFactory.Builder();
        threadfactorybuilder.uncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                err.println("uncaughtException method called...");
                e.printStackTrace();
                exit(SYSTEM_EXIT_UNCAUGHT_EXCEPTION_CODE_3);
            }
        });
        threadfactorybuilder.daemon(false);
        singletonThreadFactory = threadfactorybuilder.build();
    }

    public static BasicThreadFactory getThreadFactory() {
        return singletonThreadFactory;
    }
}
