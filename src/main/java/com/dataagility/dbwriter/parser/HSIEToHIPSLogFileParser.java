package com.dataagility.dbwriter.parser;

import com.dataagility.dbwriter.etc.EventAction;
import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.exception.DBWriterEndOfFileException;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.JournalEvent;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.RegexValidator;

/**
 * author: roger lau 2019
 */
public class HSIEToHIPSLogFileParser extends AuditLogFileParser {

    /*
        [SENT] start of SENT
        [RECEIVED] end of SENT   -- so peek ahead needed???
        [RECEIVED] start of RECEIVED
        "Consent :" as part of line is end of RECEIVED

     */

    private String startOfSentRule = ".+(\\[SENT\\]).*";
    private String endOfSentRule = ".+(\\[RECEIVED\\]).*";
    private String startOfReceivedRule = ".+(\\[RECEIVED\\]).*";
    private String endOfReceivedRule = ".+(Consent :).";

    private String sentJournalEventFirstLineRule = "^====== [0-3][0-9]:[0-1][0-9]:[0-9][0-9][0-9][0-9] - [0-2][0-9]:[0-6][0-9]:[0-6][0-9].[0-9][0-9][0-9]  \\[SENT\\]";
    private String validInputOrOutputMessageHeaderLineRule = "^inputMessage:MSH\\|.+|^outputMessage:MSH\\|.+";
    private String validJournalLineRule1 = "^inputMessage:MSH\\|.+";
    private String validJournalLineRule2 = "^outputMessage:MSH\\|.+";
    private String validJournalLineRule3 = "^[A-Z][A-Z][A-Z0-9]\\|.+";
    private String receivedJournalEventFirstLineRule = "^====== [0-3][0-9]:[0-1][0-9]:[0-9][0-9][0-9][0-9] - [0-2][0-9]:[0-6][0-9]:[0-6][0-9].[0-9][0-9][0-9]  (\\[RECEIVED\\])";
    private String validAckLineRule1 = "^AA:MsgControl.+";
    private String endOfReceivedJournalEventRule = ".+Consent :[Y,N]";
    private String startOfJournalEventLineRule = sentJournalEventFirstLineRule + "|" + receivedJournalEventFirstLineRule;
    private String endOfJournalEventLineRule = receivedJournalEventFirstLineRule + "|" + endOfReceivedJournalEventRule;

    private RegexValidator sentJournalEventFirstLineRegex = new RegexValidator(sentJournalEventFirstLineRule);
    private RegexValidator receivedJournalEventFirstLineRegex = new RegexValidator(receivedJournalEventFirstLineRule);
    private RegexValidator validInputOrOutputMessageHeaderLineRegex = new RegexValidator(validInputOrOutputMessageHeaderLineRule);
    private RegexValidator inboundMSHHeaderLineRegex = new RegexValidator(validJournalLineRule1);
    private RegexValidator outboundMSHHeaderLineRegex = new RegexValidator(validJournalLineRule2);
    private RegexValidator startOfJournalEventLineRegex = new RegexValidator(startOfJournalEventLineRule);
    private RegexValidator endOfJournalEventLineRegex = new RegexValidator(endOfJournalEventLineRule);

    private boolean outputMessageFound = false;
    private Logger log = new Logger();

    public HSIEToHIPSLogFileParser() throws DBWriterException {
        //set up the validJournalLineRegex
        StringBuilder validLineRule = new StringBuilder();
        validLineRule.append(sentJournalEventFirstLineRule).append("|");
        validLineRule.append(validJournalLineRule1).append("|");
        validLineRule.append(validJournalLineRule2).append("|");
        validLineRule.append(validJournalLineRule3).append("|");
        validLineRule.append(receivedJournalEventFirstLineRule).append("|");
        validLineRule.append(endOfReceivedJournalEventRule).append("|");
        validLineRule.append(validAckLineRule1);
        validJournalLineRegex = new RegexValidator(validLineRule.toString());

        //set up the validAckLineRegex
        StringBuilder validAckLineRule = new StringBuilder();
        validAckLineRule.append(receivedJournalEventFirstLineRule).append("|");
        validAckLineRule.append(validAckLineRule1);
    }

    /*
        todo writeJournalEvent method in base class which takes a start & end of block regexValidator
          and returns the block of lines to extract the model out of
          Developers can overload this method and handle their own reading of audit log file line by line using
          readLine() method

        todo - thoughts...
          do we need to make this run in a thread - one for each audit log file?
          each thread produces JournalEvent objects
          they can be pushed onto a queue which is serviced by a separate thread which has a single database connection
          and as it finds JournalEvents on the queue writes them (in batches?) to the HSIE database
     */
    //    @Override
    public void parseFile() throws DBWriterException {  //or throw a new exception EventBlockReadException

        JournalEvent journalEvent = null;
        try {
            do {
                /*
                    todo consolidate this do-while block
                        move the above code in here so that we don't have a special firstline handling section
                        all audit log file lines should be handled in this same code block
                        The intent of this block is to
                        1. read a line
                        2. identify the line
                        3. do some specific things to the line depending on it's type
                        4. update the journalEvent object with data
                        5. check if we've reached the end-of-journal-marker - and save journalEvent to the list
                        6. or throw an error if we never find it?
                        7. handle the end-of-file gracefully - use this to wait for THREAD_WAIT_PERIOD
                 */

                //parseNextJournalEvent()

                String line = readLineWithEndOfLineCRLF();
                if (isInvalidJournalLine(line)) {
                    //we ignore these lines and continue reading lines
                    continue;
                }

                /* todo change this method to be isStartOrEndOfJournalEventLine(line)
                        1. check if start of event - create new journalEvent - set action - extract timestamp - continue
                        2. check if end of event - mark it - push journalEvent onto list - continue

                 */
                if (isStartOfJournalEventLine(line)) {

                    EventAction action = isSentJournalEventFirstLine(line) ? EventAction.SENT : EventAction.RECEIVED;

                    if (journalEvent != null) {
                        if (journalEvent.getMessageSentOrReceived().equals(oppositeEventAction(action))) {
                            journalEvent.setFoundEndOfJournalMarker(true);
//                            threadsafeEventQueue.add(journalEvent);
                            journalEvent = null;
                        } else {
                            throw new DBWriterException("Unexpected journal event line");
                        }
                    }
                    journalEvent = new JournalEvent();
                    journalEvent.setMessageSentOrReceived(action);
                    extractDateTimeFromFirstLine(line, journalEvent);
                    continue;
                } else if (endOfJournalEventLine(line)) {

                    //there should be a pair of journal events (1 SENT 1 RECEIVED) - get them back - calculate stuff and save it to the first (SENT) journal
                    //then post the pair on the queue to be written to database
                    //reset journalEvent to null - continue

                    extractConsent(line, journalEvent);
                    journalEvent.setFoundEndOfJournalMarker(true);
//                    threadsafeEventQueue.add(journalEvent);
                    journalEvent = null;
                    continue;
                }

                if (isInboundMSHHeaderLine(line)) {
                    if (line.startsWith("input"))
                        line = StringUtils.substringAfter(line, "inputMessage:");
                    outputMessageFound = false;
                } else if (isOutboundMSHHeaderLine(line)) {
                    if (line.startsWith("output"))
                        line = StringUtils.substringAfter(line, "outputMessage:");
                    outputMessageFound = true;
                }

                //todo are we handling if we haven't finished a full JournalEvent - writeJournalEvent this to the database as INCOMPLETE_EVENT_ERROR???

                parseLine(line, journalEvent);

            } while (true);

        } catch (DBWriterEndOfFileException e) {
            //reached end of file - handle this gracefully
            log.info("reached end of file");
            //check if we've found the end of journal marker - if we have then push journalEvent onto the list and wait for THREAD_WAIT_PERIOD
            //if we've not found the end of journal marker - throw a DBWriterException to return to DBWriterMarch2019 call - which should writeJournalEvent this INCOMPLETE_EVENT_ERROR to the stderr
            //todo here we can wait for THREAD_WAIT_PERIOD - eg wait for 5 mins
        }

    }

    private void parseLine(String line, JournalEvent journalEvent) {

         /*//split the segment raw line into fields array
        String[] fieldsArray = StringUtils.splitPreserveAllTokens(line, '|');

        if (fieldsArray.length == 0)
            throw new AnalyseLogException("bad segment line:" + line);

        //record the segment name
        String segmentName = fieldsArray[0];

        if (isMessageHeaderLine(line)) {

            //check for ACK acknowledgement messages
            if (isAckMessageLine(line)) {
                countAcks++;
                currentJournalEvent = new HL7Message("ACK");
                currentJournalEvent.setMessageId(fieldsArray[9]);
                threadsafeEventQueue.add(currentJournalEvent);
                return;
            }

            if (isNackMessageLine((fieldsArray))) {
                countNacks++;
                currentJournalEvent = new HL7Message("NACK");
                currentJournalEvent.setMessageId(fieldsArray[9]);
                threadsafeEventQueue.add(currentJournalEvent);
                return;
            }

            //extract the event type and if eventType filtering is switched on do filtering
            currentEventType = StringUtils.substringAfter(fieldsArray[8], "^");
            journalEventCount++;

            //from here we are good to retrieve HL7Message and process this line
            currentJournalEvent = new HL7Message(currentEventType);
            threadsafeEventQueue.add(currentJournalEvent);
        }


        //go get the segmentModel - or create a new one if it doesn't exist
        SegmentModel currentSegmentModel = new SegmentModel(segmentName);
        currentSegmentModel.setLineRaw(line);

        //now go through all the fields for this segment line and record all non-null values
        for (int fieldPosNum = 0; fieldPosNum < fieldsArray.length; fieldPosNum++) {
            FieldPositionModel currFieldPositionModel = new FieldPositionModel(fieldPosNum);

            if (!currentSegmentModel.getFieldPositionModelList().isEmpty() && currentSegmentModel.getFieldPositionModelList().contains(currFieldPositionModel)) {
                //here we replace the temporary FieldPositionModel with the cached fieldPositionModel - it has the count of values
                currFieldPositionModel = currentSegmentModel.getFieldPositionModelList().get(fieldPosNum);
            } else {
                currentSegmentModel.getFieldPositionModelList().add(currFieldPositionModel);
            }

            if (fieldPosNum == 0) {
                //we ignore fieldPosition 0 - this is always the segment name so not a valid field value
                continue;
            }

            //do this for all non-zero field positions
            String fieldValue = fieldsArray[fieldPosNum];
            if (StringUtils.isBlank(fieldValue))
                continue;

            FieldValueModel currFieldValueModel = new FieldValueModel(fieldValue);
            if (currFieldPositionModel.getFieldValueModelList().contains(currFieldValueModel)) {

                //find that FieldValueModel and add the value - thus incrementing the value count
                Iterator<FieldValueModel> iterator = currFieldPositionModel.getFieldValueModelList().iterator();
                while (iterator.hasNext()) {
                    FieldValueModel valueModel = iterator.next();
                    if (valueModel.equals(currFieldValueModel)) {
                        //call to setValue increments the value counter
                        valueModel.setValue(currFieldValueModel.getValue());
                    }
                }
            } else {
                //this fieldvaluemodel isn't included in the Set of field values - so add it now
                currFieldPositionModel.getFieldValueModelList().add(currFieldValueModel);
            }
        }
        currentJournalEvent.getSegmentModelList().add(currentSegmentModel);*/

    }


    @Override
    public boolean isMessageHeaderLine(String line) {
        String segmentName = StringUtils.substringBefore(line, "|");
        return (segmentName.compareTo("MSH") == 0);
    }

    @Override
    public boolean isInvalidJournalLine(String line) {
        if (validJournalLineRegex.isValid(line))
            return false;
        else
            return true;
    }

    public boolean isSentOrReceivedJournalEventFirstLine(String line) {
        return validInputOrOutputMessageHeaderLineRegex.isValid(line);
    }

    public boolean isSentJournalEventFirstLine(String line) {
        return sentJournalEventFirstLineRegex.isValid(line);
    }

    public boolean isReceivedJournalEventFirstLine(String line) {
        return receivedJournalEventFirstLineRegex.isValid(line);
    }

    public boolean isOutboundMSHHeaderLine(String line) {
        return outboundMSHHeaderLineRegex.isValid(line);
    }

    public boolean isInboundMSHHeaderLine(String line) {
        return inboundMSHHeaderLineRegex.isValid(line);
    }

    public boolean isStartOfJournalEventLine(String line) {
        return startOfJournalEventLineRegex.isValid(line);
    }

    public boolean endOfJournalEventLine(String line) {
        return endOfJournalEventLineRegex.isValid(line);
    }

    private void extractConsent(String line, JournalEvent journalEvent) {

    }

}
