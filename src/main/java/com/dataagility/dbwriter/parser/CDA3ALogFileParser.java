package com.dataagility.dbwriter.parser;

import com.dataagility.dbwriter.etc.DBWSequence;
import com.dataagility.dbwriter.etc.EventAction;
import com.dataagility.dbwriter.etc.Utils;
import com.dataagility.dbwriter.exception.DBWriterEndOfFileException;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.HL7Message;
import com.dataagility.dbwriter.model.JournalEvent;
import com.dataagility.dbwriter.model.JournalEventsPair;
import com.dataagility.dbwriter.model.SegmentModel;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.RegexValidator;

import static com.dataagility.dbwriter.etc.Utils.isNotEmpty;
import static com.dataagility.dbwriter.etc.Utils.isEmpty;
import static com.dataagility.dbwriter.etc.Utils.findIdRootValue;
import static com.dataagility.dbwriter.etc.Utils.findDocumentOIDValue;
import static com.dataagility.dbwriter.etc.Utils.findDocumentSetIDValue;
import static com.dataagility.dbwriter.etc.Utils.findXmlValueByElementName;

import static org.apache.commons.lang.StringUtils.substringBefore;
import static org.apache.commons.lang.StringUtils.substringAfter;
import static org.apache.commons.lang.StringUtils.splitPreserveAllTokens;


/**
 * author: roger lau 2019
 */
public class CDA3ALogFileParser extends AuditLogFileParser {

    private final static String serviceName = "HIPS_CDA3A";

    private final String sentJournalEventFirstLineRule = "^====== [0-3][0-9]:[0-1][0-9]:[0-9][0-9][0-9][0-9] - [0-2][0-9]:[0-6][0-9]:[0-6][0-9].[0-9][0-9][0-9]  \\[SENT\\]";
    private final String endOfSentOrReceivedJournalEventRule = "^$";
    private final String receivedJournalEventFirstLineRule = "^====== [0-3][0-9]:[0-1][0-9]:[0-9][0-9][0-9][0-9] - [0-2][0-9]:[0-6][0-9]:[0-6][0-9].[0-9][0-9][0-9]  (\\[RECEIVED\\])";
    private final String validHL7SegmentLineRule = "^[A-Z][A-Z][A-Z0-9]\\|.+";
    private final String messageControlIdRule = "^MessageControlId=.+";
    private final String inputHL7MessageStartLineRule = "^INFO \\(InputMessage\\):.+";
    private final String infoIHILineRule = "^INFO \\(IHI\\):.+";
    private final String submittedCDALineRule = "^SubmittedCDA=.+";
    private final String parameterNameLineRule = "Parameter name:";
    private final String errorCodeLineRule = "^Error=Code::.+";
    private final String errorDescriptionLineRule = "^Error=Description::.+";

    private final RegexValidator sentJournalEventFirstLineRegex = new RegexValidator(sentJournalEventFirstLineRule);
    private final RegexValidator receivedJournalEventFirstLineRegex = new RegexValidator(receivedJournalEventFirstLineRule);
    private final RegexValidator messageControlIdRegex = new RegexValidator(messageControlIdRule);
    private final RegexValidator inputHL7MessageStartLineRegex = new RegexValidator(inputHL7MessageStartLineRule);
    private final RegexValidator infoIHILineRegex = new RegexValidator(infoIHILineRule);
    private final RegexValidator validHL7SegmentLineRegex = new RegexValidator(validHL7SegmentLineRule);
    private final RegexValidator endOfJournalEventLineRegex = new RegexValidator(endOfSentOrReceivedJournalEventRule);
    private final RegexValidator errorCodeLineRegex = new RegexValidator(errorCodeLineRule);
    private final RegexValidator errorDescriptionLineRegex = new RegexValidator(errorDescriptionLineRule);
    private final RegexValidator submittedCDALineRegex = new RegexValidator(submittedCDALineRule);
    private final RegexValidator parameterNameLineRegex = new RegexValidator(parameterNameLineRule);

    private HL7Message currentHL7Message;
    private String currentEventType;

    public CDA3ALogFileParser() throws DBWriterException {

        //set up the validJournalLineRegex
        StringBuilder validLineRule = new StringBuilder();
        validLineRule.append(sentJournalEventFirstLineRule).append("|");
        validLineRule.append(receivedJournalEventFirstLineRule).append("|");
        validLineRule.append(messageControlIdRule).append("|");
        validLineRule.append(inputHL7MessageStartLineRule).append("|");
        validLineRule.append(validHL7SegmentLineRule).append("|");
        validLineRule.append(errorCodeLineRule).append("|");
        validLineRule.append(errorDescriptionLineRule).append("|");
        validLineRule.append(submittedCDALineRule).append("|");
        validLineRule.append(infoIHILineRule).append("|");
        validLineRule.append(parameterNameLineRule);
        validJournalLineRegex = new RegexValidator(validLineRule.toString());
    }

    public void parseFile() throws DBWriterException, DBWriterEndOfFileException {

        JournalEvent currentJournalEvent = null;
        JournalEventsPair currentJournalEventsPair = null;

        try {
            do {

                String line = readLineWithEndOfLineCR();
                if (isInvalidJournalLine(line)) {
                    //we ignore these lines and continue reading lines
                    continue;
                }

                if (isStartOfJournalEventLine(line)) {

                    EventAction action = isSentJournalEventFirstLine(line) ? EventAction.SENT : EventAction.RECEIVED;

                    currentJournalEvent = new JournalEvent();
                    currentJournalEvent.setMessageSentOrReceived(action);
                    currentJournalEvent.setServiceName(serviceName);
                    currentJournalEvent.setEventService(serviceName);
                    currentJournalEvent.setAuditId(DBWSequence.getNextAuditId());
                    currentJournalEvent.setEventId(DBWSequence.getNextEventId());
                    currentJournalEvent.setArchiveId(DBWSequence.getNextArchiveId());
                    currentJournalEvent.setRecordDatetime(System.currentTimeMillis());
                    currentJournalEvent.setAckResponseSentDatetime(extractDateTimeFromFirstLine(line, currentJournalEvent));

                    if (currentJournalEventsPair == null) {
                        currentJournalEventsPair = new JournalEventsPair();
                        /*
                            due to java references - we can add the new currentJournalEventsPair to the pairs list now
                            it's the same as adding it to the pairs list after both internal journal events have been added
                            to the currentJournalEventsPair
                         */
                        threadsafeEventQueue.add(currentJournalEventsPair);
                    }

                    /*
                        again we are adding the currentJournalEvent to the new journalEventsPair early
                        because java references allow this. It's cleaner than adding it later
                     */
                    switch (currentJournalEvent.getMessageSentOrReceived()) {
                        case SENT:
                            currentJournalEventsPair.setSentJournalEvent(currentJournalEvent);
                            break;
                        case RECEIVED:
                            currentJournalEventsPair.setReceivedJournalEvent(currentJournalEvent);
                    }
                    continue;

                } else if (endOfJournalEventLine(line, currentJournalEvent.getMessageSentOrReceived())) {

                    currentJournalEvent.setFoundEndOfJournalMarker(true);
                    currentJournalEvent = null;

                    if (currentJournalEventsPair != null && currentJournalEventsPair.containsBothJournalEvents()) {
                        currentJournalEventsPair = null;
                    }
                    continue;
                }

                if (messageControlIdRegex.isValid(line)) {
                    currentJournalEvent.setMessageControlId(substringAfter(line, "MessageControlId="));
                    continue;
                } else if (inputHL7MessageStartLineRegex.isValid(line)) {
                    String mshLine = substringAfter(line, "INFO (InputMessage):").trim();
                    parseHL7SegmentLine(mshLine, currentJournalEvent);
                } else if (validHL7SegmentLineRegex.isValid(line))
                    parseHL7SegmentLine(line, currentJournalEvent);
                else if (StringUtils.isEmpty(line)) {
                    //we've found the Sent Middle empty line - so now set the flag to true
                    foundSentMiddleEmptyLine = true;
                    continue;
                } else if (errorCodeLineRegex.isValid(line)) {
                    parseErrorCodeLine(line, currentJournalEvent);
                } else if (errorDescriptionLineRegex.isValid(line)) {
                    parseErrorDescriptionLine(line, currentJournalEvent);
                } else if (submittedCDALineRegex.isValid(line)) {
                    parseSubmittedCDALine(line, currentJournalEvent);
                } else if (parameterNameLineRegex.isValid(line)) {
                    parseParameterNameLine(line, currentJournalEvent);
                } else if (infoIHILineRegex.isValid(line)) {
                    parseInfoIHILine(line, currentJournalEvent);
                }


            } while (true);

        } catch (DBWriterEndOfFileException e) {
            //reached end of file - handle this gracefully
            //we MAY NOT HAVE HANDLED the last journal event and the journal events pair
            //so we do it now if it's needed
            if (currentJournalEvent != null && !currentJournalEvent.hasFoundEndOfJournalMarker()) {
                currentJournalEvent.setFoundEndOfJournalMarker(true);
                currentJournalEvent = null;
            }

            if (currentJournalEventsPair != null && currentJournalEventsPair.containsBothJournalEvents()) {
                currentJournalEventsPair = null;
            }

            //rethrow the DBWriterEndOfFileException so that the LogFileParserRunnable puts the thread to sleep
            throw e;
        }

    }


    private void parseErrorCodeLine(String line, JournalEvent journalEvent) throws DBWriterException {
        journalEvent.setErrorCode(substringAfter(line, "Error=Code::"));
        journalEvent.setEventNotes(substringAfter(line, "Error=") + "\r");
    }

    private void parseErrorDescriptionLine(String line, JournalEvent journalEvent) throws DBWriterException {
        String errorLine = substringAfter(line, "Error=");
        journalEvent.setResultStatus(substringAfter(substringAfter(errorLine, ";"), "Status::"));
        journalEvent.setErrorMessage(substringAfter(substringBefore(errorLine, ";"), "Description::"));
        journalEvent.setEventNotes(errorLine);
    }

    private void parseSubmittedCDALine(String line, JournalEvent journalEvent) throws DBWriterException {
        String cdaXml = substringAfter(line, "SubmittedCDA=");
        journalEvent.setLogMsg(cdaXml);
        journalEvent.setDocumentId(findIdRootValue(cdaXml));
        journalEvent.setDocumentOid(findDocumentOIDValue(cdaXml));
        journalEvent.setDocumentSetId(findDocumentSetIDValue(cdaXml));
        journalEvent.setDocumentType(findXmlValueByElementName("title", cdaXml));
    }

    private void parseParameterNameLine(String line, JournalEvent journalEvent) throws DBWriterException {
        String afterString = substringAfter(line, "Parameter name:");
        journalEvent.setEventNotes(afterString);
        String[] fieldsArray = splitPreserveAllTokens(afterString, ";");
        if(fieldsArray.length == 3) {
            journalEvent.setErrorMessage(substringAfter(fieldsArray[1], "Description::"));
            journalEvent.setResultStatus(substringAfter(fieldsArray[2], "Status::"));
        } else {
            journalEvent.setErrorMessage(afterString);
            journalEvent.setResultStatus(substringBefore(fieldsArray[2], "Status::"));
        }

        journalEvent.setEventNotes(afterString);
    }

    private void parseInfoIHILine(String line, JournalEvent journalEvent) throws DBWriterException {
        String IHIString = substringAfter(line, "INFO (IHI):");
        journalEvent.setIhi(IHIString);
    }

    private void parseHL7SegmentLine(String line, JournalEvent journalEvent) throws DBWriterException {

        //split the segment raw line into fields array
        String[] fieldsArray = splitPreserveAllTokens(line, '|');

        if (fieldsArray.length == 0)
            throw new DBWriterException("bad segment line:" + line);

        //record the segment name
        String segmentName = fieldsArray[0];
        if (isEmpty(segmentName))
            throw new DBWriterException("bad segment name");
        else
            segmentName = segmentName.trim();

        if (segmentName.matches("MSH")) {
            //extract the event type and if eventType filtering is switched on do filtering
            currentEventType = substringBefore(fieldsArray[8], "^");
            journalEvent.setMessageType(currentEventType);
            journalEvent.setMessageEvent(substringAfter(fieldsArray[8], "^"));
            //from here we are good to retrieve HL7Message and process this line
            currentHL7Message = new HL7Message(currentEventType);
            currentHL7Message.setMessageId(fieldsArray[9]);
            journalEvent.setHl7Message(currentHL7Message);
            journalEvent.setSendingApp(fieldsArray[2]);
            journalEvent.setSendingFacility(fieldsArray[3]);
            journalEvent.setReceivingApp(fieldsArray[4]);
            journalEvent.setReceivingFacility(fieldsArray[5]);
            journalEvent.setMessageDateTime(Utils.stripOffTimezone(fieldsArray[6]));
            journalEvent.setProcessingId(fieldsArray[10]);
            journalEvent.setVersionId(fieldsArray[11]);
            journalEvent.setCharacterSet(fieldsArray[17]);

        } else if (segmentName.matches("EVN")) {
            journalEvent.setEventDatetime(fieldsArray[2]);
        } else if (segmentName.matches("PID")) {
            journalEvent.setPatientId(fieldsArray[3]);
            journalEvent.setPatientUr(Utils.getSubFieldFromFieldByNumber(1, fieldsArray[3]));
            journalEvent.setPatientGivenName(Utils.getSubFieldFromFieldByNumber(1, fieldsArray[5]));
            journalEvent.setPatientFamilyName(Utils.getSubFieldFromFieldByNumber(2, fieldsArray[5]));
            String dobString = fieldsArray[7];
            if (isNotEmpty(dobString))
                journalEvent.setPatientDob(dobString.substring(0, 8));
            journalEvent.setPatientSex(fieldsArray[8]);
            journalEvent.setPatientAddress(fieldsArray[11]);
        } else if (segmentName.matches("PV1")) {
            journalEvent.setPatientClass(fieldsArray[2]);
            journalEvent.setVisitId(Utils.getSubFieldFromFieldByNumber(1, fieldsArray[19]));
            journalEvent.setAdmitDatetime(fieldsArray[44]);
            journalEvent.setDischargeDatetime(Utils.stripOffTimezone(fieldsArray[45]));
        } else if (segmentName.matches("OBR")) {
            journalEvent.setOrderNumber(Utils.getSubFieldFromFieldByNumber(1, fieldsArray[3]));
            journalEvent.setResultStatus(fieldsArray[25]);
            journalEvent.setObservationDate(Utils.stripOffTimezone(fieldsArray[7]));
        }

        //go get the segmentModel - or create a new one if it doesn't exist
        SegmentModel currentSegmentModel = new SegmentModel(segmentName);
        currentSegmentModel.setLineRaw(line);
        currentHL7Message.getSegmentModelList().add(currentSegmentModel);
    }


    @Override
    protected boolean isInvalidJournalLine(String line) {
        if (validJournalLineRegex.isValid(line))
            return false;
        else
            return true;
    }

    protected boolean isSentJournalEventFirstLine(String line) {
        return sentJournalEventFirstLineRegex.isValid(line);
    }

    protected boolean isReceivedJournalEventFirstLine(String line) {
        return receivedJournalEventFirstLineRegex.isValid(line);
    }

    public boolean isStartOfJournalEventLine(String line) {
        if (sentJournalEventFirstLineRegex.isValid(line) || receivedJournalEventFirstLineRegex.isValid(line))
            return true;
        else
            return false;
    }

}
