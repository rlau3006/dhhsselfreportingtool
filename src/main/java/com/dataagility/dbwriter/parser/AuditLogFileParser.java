package com.dataagility.dbwriter.parser;

import com.dataagility.dbwriter.etc.EventAction;
import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.etc.Utils;
import com.dataagility.dbwriter.exception.DBWriterEndOfFileException;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.JournalEvent;
import com.dataagility.dbwriter.thread.JournalEventThreadsafeDoubleEndedQueue;
import org.apache.commons.io.FileUtils;
import org.apache.commons.validator.routines.RegexValidator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParsePosition;
import java.util.Date;

import static com.dataagility.dbwriter.etc.Utils.sentReceivedLineDatetimeFormat;
import static org.apache.commons.lang.StringUtils.*;

/**
 * author: roger lau 2019
 */
public abstract class AuditLogFileParser {

    /**
     * <Component type="ProcessPcehrExist">
     * <Start marker="======" type="[SENT]" datetimefmt="dd:mm:yyyy – HH:MM:ss:SSS" order="1">
     * <Properties>
     * <Property name="MessageControlID" sep=" = " type="Simple"/>
     * <Property name="SendingFacility" sep=" = " type="Simple"/>
     * <Property name="FoundMRN" sep=" = " type="List"/>
     * <Property name="DateOfBirth" sep=" = " type="Date"/>
     * </Properties>
     * </Start>
     * <End marker="======" type="[RECEIVED]" datetimefmt="dd:mm:yyyy – HH:MM:ss:SSS" order="2">
     * <Properties>
     * <Property name="Error" sep=" = " type="Error"/>
     * <Property name="RepeatedErrorInfo" sep=" = " type="List"/>
     * <Property name="Result" sep=" = " type="Simple"/>
     * <Property name="AcceptedMRN" sep=" = " type="String"/>
     * </Properties>
     * </End>
     * </Component>
     */

    protected String serviceName = null;
    protected String logfileName;
    protected File logfile;
    protected FileInputStream fileInputStream;
    private Logger log;

    protected RegexValidator validJournalLineRegex;
    protected boolean foundSentMiddleEmptyLine = false;
    JournalEventThreadsafeDoubleEndedQueue threadsafeEventQueue = null;
    protected boolean startOfStreamIndicator = false;

    public AuditLogFileParser() {
    }

    public void setLogFile(String dirname, String logFileNamePrefix) throws DBWriterException {

        String currentDateStr = Utils.getCurrentDateInLogFileFormat();
        StringBuilder filenameStrBldr = new StringBuilder().append(dirname).append("\\\\").append(logFileNamePrefix).append("_").append(currentDateStr).append(".log");
        this.logfileName = filenameStrBldr.toString();
        try {
            this.logfile = new File(logfileName);
            this.fileInputStream = FileUtils.openInputStream(this.logfile);
        } catch (IOException e) {
            throw new DBWriterException(e);
        }
    }

    public String getFilename() {
        if ( logfile == null)
            return this.logfileName;
        else
            return this.logfile.getName();
    }

    public abstract void parseFile() throws DBWriterException, DBWriterEndOfFileException;

    protected boolean isMessageHeaderLine(String line) {
        return isSegmentByName(line, "MSH");
    }

    protected boolean isEVNSegment(String line) {
        return isSegmentByName(line, "EVN");
    }

    protected boolean isPIDSegment(String line) {
        return isSegmentByName(line, "PID");
    }

    protected boolean isPV1Segment(String line) {
        return isSegmentByName(line, "PV1");
    }

    protected boolean isOBRSegment(String line) {
        return isSegmentByName(line, "OBR");
    }

    protected boolean isSegmentByName(String line, String segmentName) {
        String segmentNameExtracted = substringBefore(line, "|");
        return (segmentName.compareTo(segmentNameExtracted) == 0);
    }

    protected abstract boolean isInvalidJournalLine(String line);

    /**
     * Reads a line from the audit log file. Log file is of course is set by method setLogFile(String logfilename)
     * This method handles both Windows files and Unix files in terms of line ending characters.
     * No support for Mac files (unfortunately)
     *
     * @return
     * @throws DBWriterEndOfFileException
     * @throws DBWriterException
     */
    protected String readLineWithEndOfLineCRLF() throws DBWriterEndOfFileException, DBWriterException {

        StringBuffer strBuffer = new StringBuffer();
        char c;
        try {
            do {
                c = (char) fileInputStream.read();
                if ( c == '\n' )
                    return strBuffer.toString().trim();
                else if ( c != '\r' )
                    strBuffer.append(c);
            }
            while ( c != (char) -1 );
            throw new DBWriterEndOfFileException();

        } catch ( IOException ioe ) {
            throw new DBWriterException("error reading from audit log file");
        }
    }

    protected String readLineWithEndOfLineCR() throws DBWriterEndOfFileException, DBWriterException {

        StringBuffer strBuffer = new StringBuffer();
        char c;
        try {
            do {
                c = (char) fileInputStream.read();
                if ( c == '\r' )
                    return strBuffer.toString().trim();
                else if ( c != '\n' )
                    strBuffer.append(c);
            }
            while ( c != (char) -1 );
            throw new DBWriterEndOfFileException();

        } catch ( IOException ioe ) {
            throw new DBWriterException("error reading from audit log file");
        }
    }

    public void setThreadsafeEventQueue(JournalEventThreadsafeDoubleEndedQueue threadsafeEventQueue) {
        this.threadsafeEventQueue = threadsafeEventQueue;
    }

    public void close() {
        try {
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected EventAction oppositeEventAction(EventAction action) {
        return action.equals(EventAction.SENT) ? EventAction.RECEIVED : EventAction.SENT;
    }

    protected long extractDateTimeFromFirstLine(String firstLine, JournalEvent journalEvent) {
        if (journalEvent.getMessageSentOrReceived().equals(EventAction.SENT) || journalEvent.getMessageSentOrReceived().equals(EventAction.RECEIVED)) {
            String dateStr = substringAfter(firstLine, "======");
            dateStr = substringBefore(dateStr, "[").trim();
//            log.info("parsing dateStr:[" + dateStr + "]");
            Date tmpDate = null;
            try {
                tmpDate = sentReceivedLineDatetimeFormat.parse(dateStr, new ParsePosition(0));
            } catch (Exception e) {
                return 0;
            }
            return tmpDate.getTime();
        } else
            return 0;
    }

    protected boolean endOfJournalEventLine(String line, EventAction action) {
        if (action.equals(EventAction.SENT)) {
            if (foundSentMiddleEmptyLine && isEmpty(line)) {
                //we've used the flag, so now reset it back to false
                foundSentMiddleEmptyLine = false;
                return true;
            } else
                return false;
        } else
            return isEmpty(line);
    }

    public void setStartOfStreamIndicator(boolean startOfStreamIndicator) {
        this.startOfStreamIndicator = startOfStreamIndicator;
    }
}
