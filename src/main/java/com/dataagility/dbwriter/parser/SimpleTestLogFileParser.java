package com.dataagility.dbwriter.parser;

import com.dataagility.dbwriter.exception.DBWriterEndOfFileException;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.thread.JournalEventThreadsafeDoubleEndedQueue;
import org.apache.commons.lang.StringUtils;

/**
 * author: roger lau 2019
 */
public class SimpleTestLogFileParser extends AuditLogFileParser {

    public SimpleTestLogFileParser() {
    }

    public void parseFile() throws DBWriterException, DBWriterEndOfFileException {

        do {

            String line = readLineWithEndOfLineCR();
            System.out.println(line);

        } while (true);

    }

    @Override
    protected boolean isMessageHeaderLine(String line) {
        String segmentName = StringUtils.substringBefore(line, "|");
        return (segmentName.compareTo("MSH") == 0);
    }

    @Override
    protected boolean isInvalidJournalLine(String line) {
        if (validJournalLineRegex.isValid(line))
            return false;
        else
            return true;
    }
}
