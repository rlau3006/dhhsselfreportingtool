package com.dataagility.dbwriter.exception;

/**
 * author: roger lau 2019
 */
public class DBWriterException extends Exception {
    public DBWriterException() {
    }

    public DBWriterException(String message) {
        super(message);
    }

    public DBWriterException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBWriterException(Throwable cause) {
        super(cause);
    }
}
