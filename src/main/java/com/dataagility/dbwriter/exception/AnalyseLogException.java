package com.dataagility.dbwriter.exception;

/**
 * author: roger lau 2019
 */
public class AnalyseLogException extends Exception {
    public AnalyseLogException() {
    }

    public AnalyseLogException(String message) {
        super(message);
    }

    public AnalyseLogException(String message, Throwable cause) {
        super(message, cause);
    }

    public AnalyseLogException(Throwable cause) {
        super(cause);
    }
}
