package com.dataagility.dbwriter.exception;

/**
 * author: roger lau 2019
 */
public class DBWriterEndOfFileException extends Exception {
    public DBWriterEndOfFileException() {
    }

    public DBWriterEndOfFileException(String message) {
        super(message);
    }

    public DBWriterEndOfFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBWriterEndOfFileException(Throwable cause) {
        super(cause);
    }
}
