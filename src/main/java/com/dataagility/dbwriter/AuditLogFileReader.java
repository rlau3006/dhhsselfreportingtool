package com.dataagility.dbwriter;

import com.dataagility.dbwriter.exception.DBWriterException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * author: roger lau
 */
//todo delete class AuditLogFileReader
public class AuditLogFileReader {

    String logfileName;
    File logFile;
    Iterator<String> linesIterator;

    public AuditLogFileReader(String logfilename) throws DBWriterException {
        this.logfileName = logfilename;
        this.logFile = new File(logfilename);
        try {
            linesIterator = FileUtils.lineIterator(this.logFile);
        } catch (IOException e) {
            throw new DBWriterException(e);
        }
    }

    public AuditLogFileReader(File logFile) throws DBWriterException {
        this.logfileName = logFile.getName();
        this.logFile =logFile;
        try {
            linesIterator = FileUtils.lineIterator(this.logFile);
        } catch (IOException e) {
            throw new DBWriterException(e);
        }
    }

    //todo clean this old code up
    /*public boolean hasNextEventBlock() {


        return true;
    }

    public RawEventBlock getNextEventBlock() {
        return currentEventBlock;
    }*/

    public boolean hasNextLine() {
        return linesIterator.hasNext();
    }

    public String nextLine() {
        return linesIterator.next();
    }

    public String getFilename() {
        if (logFile != null)
            return logFile.getAbsolutePath();
        else
            return "";
    }
}
