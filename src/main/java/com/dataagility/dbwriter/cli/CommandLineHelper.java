package com.dataagility.dbwriter.cli;

import com.dataagility.dbwriter.etc.Logger;
import org.apache.commons.cli.*;

import java.util.Arrays;

import static com.dataagility.dbwriter.etc.Constants.SYSTEM_EXIT_BAD_COMMAND_LINE_ARG_CODE_1;
import static java.lang.System.*;

/**
 * author: roger lau 2019
 */
public class CommandLineHelper {

    public static String CONFIG_FILE_OPTION = "configFilePath";
    public static String USAGE_OPTION = "usage";
    private static Logger log = new Logger();

    public static Options generateOptions()
    {
        final Option usageOption = Option.builder("?")
                .required(false)
                .hasArg(false)
                .longOpt(USAGE_OPTION)
                .desc("Usage message.")
                .build();

        final Option configFileOption = Option.builder("c")
                .required(true)
                .longOpt(CONFIG_FILE_OPTION)
                .hasArg()
                .desc("DBWriter configuration file.")
                .build();

        final Options options = new Options();
        options.addOption(configFileOption);
        options.addOption(usageOption);
        return options;
    }

    public static CommandLine generateCommandLine(
            final Options options, final String[] commandLineArguments)
    {
        final CommandLineParser cmdLineParser = new DefaultParser();
        CommandLine commandLine = null;
        try
        {
            commandLine = cmdLineParser.parse(options, commandLineArguments);
        }
        catch (ParseException parseException)
        {
            log.info(
                    "ERROR: Unable to parseFile command-line arguments "
                            + Arrays.toString(commandLineArguments) + " due to: "
                            + parseException);
            log.info("");
            printUsage();
            exit(SYSTEM_EXIT_BAD_COMMAND_LINE_ARG_CODE_1);
        }
        return commandLine;
    }

    public static void printUsage()
    {
        StringBuilder usageBldr = new StringBuilder();
        usageBldr.append("usage: java -jar dbWriter.jar ");
        usageBldr.append("[-?] -c <full-path-to-dbwriter.cfg>");
        usageBldr.append("\n");
        usageBldr.append("\n");
        usageBldr.append("\t-c\tfull path to dbwriter config file");
        log.info(usageBldr.toString());
    }
}
