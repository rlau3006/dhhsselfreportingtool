package com.dataagility.dbwriter;

import com.dataagility.dbwriter.cli.CommandLineHelper;
import com.dataagility.dbwriter.dao.DAO;
import com.dataagility.dbwriter.dao.DBHelper;
import com.dataagility.dbwriter.dao.DatabaseWriterDAO;
import com.dataagility.dbwriter.etc.Constants;
import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.parser.AuditLogFileParser;
import com.dataagility.dbwriter.thread.JournalEventThreadsafeDoubleEndedQueue;
import com.dataagility.dbwriter.thread.JounalEventsQueueListenerRunnable;
import com.dataagility.dbwriter.thread.LogFileParserRunnable;
import com.dataagility.dbwriter.thread.ThreadFactoryHelper;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.nio.charset.Charset;
import java.util.*;

import static com.dataagility.dbwriter.cli.CommandLineHelper.CONFIG_FILE_OPTION;
import static com.dataagility.dbwriter.etc.Constants.*;
import static java.lang.System.*;

/**
 * author: roger lau 2019
 */
public class DBWriter {

    private static JournalEventThreadsafeDoubleEndedQueue queueThatsThreadsafe = new JournalEventThreadsafeDoubleEndedQueue();
    private static DAO databaseWriterDAO = new DatabaseWriterDAO();
    private static JounalEventsQueueListenerRunnable queueListenerRunnable = new JounalEventsQueueListenerRunnable(queueThatsThreadsafe, databaseWriterDAO);
    private static Map<String, Thread> logFileParserThreadList = new HashMap<>();
    private static Date currentDate = null;

    private static File applicationConfigFile = null;
    private static List<AuditLogFileParserThreadConfiguration> threadStartupConfigurationLines = new ArrayList<>();
    private static File logfile = null;
    private static boolean filelogging = false;
    private static Logger log = new Logger();


    public static void main(String[] args) throws IOException, DBWriterException {

        initCmdLine(args);

        initCurrentDate();

        initAuditLogFileParserThreadConfigurations();

        log.writeToFileAndStdout("Data Agility Exception Reporting Tool");
        log.writeToFileAndStdout("starting...\n");

        startupAuditLogFileParserThreads();

        startupQueueListenerThread();

        try {
            while (true) {
                Thread.sleep(DEFAULT_MAIN_THREAD_SLEEP_TIME_IN_MILLISECONDS);
                log.writeToFileAndStdout("DBWriter thread awake");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void startupQueueListenerThread() {
        Thread queueListenerThread = ThreadFactoryHelper.getThreadFactory().newThread(queueListenerRunnable);
        queueListenerThread.setName("QueueListenerThread");
        queueListenerThread.start();
    }

    private static void initCmdLine(String[] args) {
        Options options = CommandLineHelper.generateOptions();
        CommandLine cmdline = CommandLineHelper.generateCommandLine(options, args);

        if (cmdline.hasOption(CONFIG_FILE_OPTION)) {
            applicationConfigFile = new File(cmdline.getOptionValue(CONFIG_FILE_OPTION));
        }
    }

    private static void initCurrentDate() {
        currentDate = new Date(currentTimeMillis());
    }

    public static Date getCurrentDate() {
        return currentDate;
    }

    private static void initAuditLogFileParserThreadConfigurations() throws IOException {
        LineIterator lineIterator = IOUtils.lineIterator(new FileInputStream(applicationConfigFile), (Charset)null);

        int lineCount = 0;
        while (lineIterator.hasNext()) {

            lineCount++;
            try {

                String configLine = lineIterator.nextLine();
                if (StringUtils.isEmpty(configLine) || configLine.startsWith("--"))
                    continue;

                if (configLine.startsWith("jdbcConnectionUrl=")) {
                    DBHelper.setJdbcUrl(StringUtils.substringAfter(configLine, "jdbcConnectionUrl="));
                    continue;
                } else if (configLine.startsWith("queueListenerThreadSleepDurationMilliseconds=")) {
                    queueListenerRunnable.setSleepDurationMilliseconds(StringUtils.substringAfter(configLine, "queueListenerThreadSleepDurationMilliseconds="));
                    continue;
                } else if (configLine.startsWith("logfile=")) {
                    log.initLogger(StringUtils.substringAfter(configLine, "logfile="));
                    continue;
                }

                AuditLogFileParserThreadConfiguration threadConfigLine = new AuditLogFileParserThreadConfiguration(configLine, lineCount);
                threadStartupConfigurationLines.add(threadConfigLine);

            } catch (DBWriterException e) {
                e.printStackTrace();
            }
        }
    }

    private static void startupAuditLogFileParserThreads() throws DBWriterException {

        Iterator<AuditLogFileParserThreadConfiguration> threadConfigurationIterator = threadStartupConfigurationLines.iterator();

        while (threadConfigurationIterator.hasNext()) {

            AuditLogFileParserThreadConfiguration threadConfiguration = threadConfigurationIterator.next();

            //now create the thread to run the log file parser
            String threadName = threadConfiguration.getParser().getFilename();
            Thread currentThread = ThreadFactoryHelper.getThreadFactory().newThread(
                    new LogFileParserRunnable(
                            threadName,
                            threadConfiguration.getSleepTimeInMilliseconds(),
                            threadConfiguration.getParser()));
            currentThread.setName(threadName);

            //now start the log file parser thread
            currentThread.start();

            logFileParserThreadList.put(threadName, currentThread);
        }
    }


    private static class AuditLogFileParserThreadConfiguration {

        private int lineNumber;
        private String parserClassnameStr;
        private String sleepTimeStr;
        private String dirPathStr;
        private String filePrefix;

        AuditLogFileParser parser;
        long sleepTimeInMilliseconds;

        public AuditLogFileParserThreadConfiguration(String dbwriterConfigLine, int lineNumber) throws DBWriterException {
            this.lineNumber = lineNumber;

            //path-to-audit-log-file,AuditLogFileParser class, sleep-period
            String[] configItemsArray = StringUtils.splitPreserveAllTokens(dbwriterConfigLine, ",");

            if(configItemsArray == null || configItemsArray.length != 4) {
                err.println("dbwriter.cfg illegal line:[" + dbwriterConfigLine + "]");
                err.println("FATAL ERROR...exiting");
                exit(SYSTEM_EXIT_BAD_CONFIG_FILE_LINE_CODE_2);
            }

            this.parserClassnameStr = configItemsArray[0];
            this.sleepTimeStr = configItemsArray[1];
            this.dirPathStr = configItemsArray[2];
            this.filePrefix = configItemsArray[3];

            //now put together the log file parser
            this.parser = buildLogFileParser(this.parserClassnameStr, lineNumber);
            this.parser.setLogFile(this.dirPathStr, this.filePrefix);
            this.sleepTimeInMilliseconds = parseSleepTimeConfigString(this.sleepTimeStr);
        }

        private AuditLogFileParser buildLogFileParser(String parserTypeClassname, int lineNumber) {

            AuditLogFileParser parser = null;
            try {
                parserTypeClassname = parserFullPackageNameWithPeriod + parserTypeClassname;
                Class<?> clazz = Class.forName(parserTypeClassname);
                Constructor constructor = clazz.getConstructor();
                parser = (AuditLogFileParser) constructor.newInstance();

            } catch (ReflectiveOperationException e) {
                err.println("FATAL_ERROR: error found in config file: [" + applicationConfigFile.getAbsolutePath() + "]");
                System.err.println("FATAL_ERROR: bad classname found at: [line " + lineNumber + "]");
                System.err.println("FATAL_ERROR: there was a problem instantiating an AuditLogFileParser using classname: [" + parserTypeClassname + "]");
                System.err.println("FATAL_ERROR: check that this is a valid classname and it exists on the classpath to this utility");
                exit(SYSTEM_EXIT_INCORRECT_CLASSNAME_IN_CONFIG_LINE_CODE_4);
            }

            parser.setThreadsafeEventQueue(queueThatsThreadsafe);
            return parser;
        }

        private long parseSleepTimeConfigString(String configString) {
            long sleepTimeInMilliseconds = Constants.DEFAULT_THREAD_SLEEP_TIME_IN_MILLISECONDS;
            try {
                sleepTimeInMilliseconds = Long.parseLong(configString);
            } catch (NumberFormatException e) {
                //do nothing - use the DEFAULT_THREAD_SLEEP_TIME_IN_MILLISECONDS
            }
            return sleepTimeInMilliseconds;
        }

        public AuditLogFileParser getParser() {
            return parser;
        }

        public long getSleepTimeInMilliseconds() {
            return sleepTimeInMilliseconds;
        }
    }
}
