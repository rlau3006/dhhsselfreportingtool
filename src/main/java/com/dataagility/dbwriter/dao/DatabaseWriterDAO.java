package com.dataagility.dbwriter.dao;

import com.dataagility.dbwriter.etc.Utils;
import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.JournalEvent;
import com.dataagility.dbwriter.model.JournalEventsPair;
import com.dataagility.dbwriter.model.SegmentModel;

import java.sql.*;
import java.util.Iterator;

/**
 * author: roger lau
 */
public class DatabaseWriterDAO implements DAO {

    private Logger log = new Logger();
    private static int audit_id = 0;

    @Override
    public void write(JournalEventsPair eventsPair) throws DBWriterException {

        JournalEvent sentJouralEvent = eventsPair.getSentJournalEvent();
        JournalEvent receivedJournalEvent = eventsPair.getReceivedJournalEvent();

        writeJournalEvent(sentJouralEvent);
        writeJournalEvent(receivedJournalEvent);
    }

    public void writeJournalEvent(JournalEvent journalEvent) throws DBWriterException {
        if (journalEvent == null)
            return;

        if(journalEvent.isStartOfStreamEvent()) {
            DBWSequenceTuple sequenceTuple = getAllNextSequenceValues();
            writeDbwAuditRow(journalEvent, sequenceTuple);
            writeDbwEventRow(journalEvent, sequenceTuple);
            writeDbwArchiveRow(journalEvent, sequenceTuple);
        } else {
            DBWSequenceTuple sequenceTuple = getEventAndArchiveNextSequenceValues();
            writeDbwEventRow(journalEvent, sequenceTuple);
            writeDbwArchiveRow(journalEvent, sequenceTuple);
            sequenceTuple.setAuditId(lookupAuditId(journalEvent));
            updateDbwAuditRow(journalEvent, sequenceTuple);
        }
    }

    public int lookupAuditId(JournalEvent journalEvent) throws DBWriterException {

        PreparedStatement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;
        int auditId = -1;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" SELECT AUDIT_ID from dbo.DBW_AUDIT ");
        sqlBuff.append(" WHERE MSG_CONTROL_ID = ? ");

        try {

            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.prepareStatement(sqlBuff.toString());
            statement.setString(1, journalEvent.getMessageControlId());

            results = statement.executeQuery();
            if ( results.next() ) {
                auditId = results.getInt(1);
            }

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(conn, statement, results);
        }
        return auditId;
    }

    //todo will need to write a few of these methods to support different parsers
    public void updateDbwAuditRow(JournalEvent journalEvent, DBWSequenceTuple tuple) throws DBWriterException {

        PreparedStatement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" UPDATE dbo.DBW_AUDIT ");
        sqlBuff.append(" SET CONSENT = ?, PCEHR_EXIST = ?, IHI = ? ");
        sqlBuff.append(" WHERE AUDIT_ID = ? ");

        //MSG_DTM,MSG_TYPE,MSG_EVENT,MSG_CONT_ID,PROCESSING_ID,VERSION_ID,CHARACTER_SET,EVENT_DTM,PATIENT_ID,PATIENT_UR,PATIENT_GIVEN_NAME
        //PATIENT_FAMILY_NAME,PATIENT_DOB,PATIENT_SEX,PATIENT_ADDRESS,PATIENT_CLASS,VISIT_ID,ADMIT_DTM,DISCHARGE_DTM,ORDER_NUMBER
        //RESULT_STATUS,OBSERVATION_DATE,DOCUMENT_ID,DOCUMENT_OID,DOCUMENT_SET_ID,DOCUMENT_TYPE,USER_ID,IHI,CONSENT,PCEHR_EXIST
        //ACK_RESP_SENT_DTM,ACK_RESP_SENT_CODE,ACK_RESP_SENT_MSG,ACK_RESP_RECD_DTM,ACK_RESP_RECD_CODE,ACK_RESP_RECD_MSG

    }

    public void writeDbwAuditRow(JournalEvent journalEvent, DBWSequenceTuple tuple) throws DBWriterException {

        PreparedStatement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" INSERT INTO DBW_AUDIT ");
        sqlBuff.append(" (AUDIT_ID,RECORD_DTM,MSG_RECEIVED_SENT,SERVICE_NAME,SENDING_APP,SENDING_FAC,RECEIVING_APP,RECEIVING_FAC,");
        sqlBuff.append("MSG_DTM,MSG_TYPE,MSG_EVENT,MSG_CONT_ID,PROCESSING_ID,VERSION_ID,CHARACTER_SET,EVENT_DTM,PATIENT_ID,PATIENT_UR,PATIENT_GIVEN_NAME,");
        sqlBuff.append("PATIENT_FAMILY_NAME,PATIENT_DOB,PATIENT_SEX,PATIENT_ADDRESS,PATIENT_CLASS,VISIT_ID,ADMIT_DTM,DISCHARGE_DTM,ORDER_NUMBER,");
        sqlBuff.append("RESULT_STATUS,OBSERVATION_DATE,DOCUMENT_ID,DOCUMENT_OID,DOCUMENT_SET_ID,DOCUMENT_TYPE,USER_ID,IHI,CONSENT,PCEHR_EXIST,");
        sqlBuff.append("ACK_RESP_SENT_DTM,ACK_RESP_SENT_CODE,ACK_RESP_SENT_MSG,ACK_RESP_RECD_DTM,ACK_RESP_RECD_CODE,ACK_RESP_RECD_MSG) ");
        sqlBuff.append(" VALUES ");
        sqlBuff.append(" (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

        try {
            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.prepareStatement(sqlBuff.toString());

            statement.setString(1, journalEvent.getAuditId());
            statement.setTimestamp(2, new Timestamp(journalEvent.getRecordDatetime()));
            statement.setString(3, journalEvent.getMessageSentOrReceived().name());
            statement.setString(4, journalEvent.getServiceName());
            statement.setString(5, journalEvent.getSendingApp());
            statement.setString(6, journalEvent.getSendingFacility());
            statement.setString(7, journalEvent.getReceivingApp());
            statement.setString(8, journalEvent.getReceivingFacility());
            statement.setString(9, journalEvent.getMessageDateTime());
            statement.setString(10, journalEvent.getMessageType());
            statement.setString(11, journalEvent.getMessageEvent());
            statement.setString(12, journalEvent.getMessageControlId());
            statement.setString(13, journalEvent.getProcessingId());
            statement.setString(14, journalEvent.getVersionId());
            statement.setString(15, journalEvent.getCharacterSet());
            statement.setString(16, journalEvent.getEventDatetime());
            statement.setString(17, journalEvent.getPatientId());
            statement.setString(18, journalEvent.getPatientUr());
            statement.setString(19, journalEvent.getPatientGivenName());
            statement.setString(20, journalEvent.getPatientFamilyName());
            statement.setString(21, journalEvent.getPatientDob());
            statement.setString(22, journalEvent.getPatientSex());
            statement.setString(23, journalEvent.getPatientAddress());
            statement.setString(24, journalEvent.getPatientClass());
            statement.setString(25, journalEvent.getVisitId());
            statement.setString(26, journalEvent.getAdmitDatetime());
            statement.setString(27, journalEvent.getDischargeDatetime());
            statement.setString(28, journalEvent.getOrderNumber());
            statement.setString(29, journalEvent.getResultStatus());
            statement.setString(30, journalEvent.getObservationDate());
            statement.setString(31, journalEvent.getDocumentId());
            statement.setString(32, journalEvent.getDocumentOid());
            statement.setString(33, journalEvent.getDocumentSetId());
            statement.setString(34, journalEvent.getDocumentType());
            statement.setString(35, journalEvent.getUserId());
            statement.setString(36, journalEvent.getIhi());
            statement.setString(37, journalEvent.getConsent());
            statement.setString(38, journalEvent.getPcehrExist());
            statement.setTimestamp(39, new Timestamp(journalEvent.getAckResponseSentDatetime()));
            statement.setString(40, journalEvent.getAckResponseSentCode());
            statement.setString(41, journalEvent.getAckResponseSentMessage());
            statement.setTimestamp(42, new Timestamp(journalEvent.getAckResponseReceivedDatetime()));
            statement.setString(43, journalEvent.getAckResponseReceivedCode());
            statement.setString(44, journalEvent.getAckResponseReceivedMessage());

            int updateCount = statement.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(statement, results);
        }
    }
    public void writeDbwEventRow(JournalEvent journalEvent, DBWSequenceTuple tuple) throws DBWriterException {

        PreparedStatement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" INSERT INTO DBW_EVENTS ");
        sqlBuff.append(" (EVENT_ID,RECORD_DTM,AUDIT_ID,EVENT_SERVICE,EVENT_NOTES) ");
        sqlBuff.append(" VALUES ");
        sqlBuff.append(" (?,?,?,?,?) ");

        try {
            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.prepareStatement(sqlBuff.toString());

            statement.setInt(1, Integer.parseInt(journalEvent.getEventId()));
            statement.setTimestamp(2, new Timestamp(journalEvent.getRecordDatetime()));
            statement.setString(3, journalEvent.getAuditId());
            statement.setString(4, journalEvent.getEventService());
            statement.setString(5, journalEvent.getEventNotes());

            int updateCount = statement.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(statement, results);
        }
    }

    public void writeDbwArchiveRow(JournalEvent journalEvent, DBWSequenceTuple tuple) throws DBWriterException {

        PreparedStatement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" INSERT INTO DBW_ARCHIVE ");
        sqlBuff.append(" (ARCHIVE_ID,RECORD_DTM,EVENT_ID,LOG_MSG,ERROR_CODE) ");
        sqlBuff.append(" VALUES ");
        sqlBuff.append(" (?,?,?,?,?) ");

        try {
            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.prepareStatement(sqlBuff.toString());

            statement.setString(1, journalEvent.getArchiveId());
            statement.setTimestamp(2, new Timestamp(journalEvent.getRecordDatetime()));
            statement.setInt(3, Integer.parseInt(journalEvent.getEventId()));

            StringBuilder logMessageStringBuilder = new StringBuilder();
            if(journalEvent.getHl7Message() != null) {
                Iterator<SegmentModel> iterator = journalEvent.getHl7Message().getSegmentModelList().iterator();
                while(iterator.hasNext()) {
                    SegmentModel currentSegmentModel = iterator.next();
                    logMessageStringBuilder.append(currentSegmentModel.getLineRaw());
                }
            } else if (Utils.isNotEmpty(journalEvent.getErrorMessage())) {
                logMessageStringBuilder.append(journalEvent.getErrorMessage());
            }
            Clob clobValue = conn.createClob();
            clobValue.setString(1, logMessageStringBuilder.toString());
            statement.setClob(4, clobValue);
            statement.setString(5, journalEvent.getErrorCode());

            int updateCount = statement.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(statement, results);
        }
    }


    @Override
    public void close() throws DBWriterException {
        try {
            DBHelper.getConnection().close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    public void writeToDBTest() throws DBWriterException {

        PreparedStatement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" INSERT INTO DBW_EVENTS ");
        sqlBuff.append(" (EVENT_ID, AUDIT_ID, EVENT_SERVICE, EVENT_NOTES) ");
        sqlBuff.append(" VALUES ");
        sqlBuff.append(" (?,?,?,?) ");

        try {


            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.prepareStatement(sqlBuff.toString());

            statement.setInt(1,3);
            statement.setInt(2,33);
            statement.setString(3,"event-service-test-value-7");
            statement.setString(4,"event-notes-test-value-67");
            statement.setInt(1, 3);
            statement.setInt(2, 33);
            statement.setString(3, "event-service-test-value-7");
            statement.setString(4, "event-notes-test-value-67");

            int updateCount = statement.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(conn, statement, results);
        }
    }

    public DBWSequenceTuple getAllNextSequenceValues() throws DBWriterException {

        DBWSequenceTuple tuple = new DBWSequenceTuple();
        Statement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" SELECT ");
        sqlBuff.append(" NEXT VALUE FOR seq_dbw_audit_id, ");
        sqlBuff.append(" NEXT VALUE FOR seq_dbw_events_id, ");
        sqlBuff.append(" NEXT VALUE FOR seq_dbw_archive_id ");

        try {

            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.createStatement();

            results = statement.executeQuery(sqlBuff.toString());
            if ( results.next() ) {
                tuple.setAuditId(results.getInt(1));
                tuple.setEventsId(results.getInt(2));
                tuple.setArchiveId(results.getInt(3));
            }

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(conn, statement, results);
        }
        return tuple;
    }

    public DBWSequenceTuple getEventAndArchiveNextSequenceValues() throws DBWriterException {

        DBWSequenceTuple tuple = new DBWSequenceTuple();
        Statement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" SELECT ");
        sqlBuff.append(" NEXT VALUE FOR seq_dbw_events_id, ");
        sqlBuff.append(" NEXT VALUE FOR seq_dbw_archive_id ");

        try {

            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.createStatement();

            results = statement.executeQuery(sqlBuff.toString());
            if ( results.next() ) {
                tuple.setEventsId(results.getInt(1));
                tuple.setArchiveId(results.getInt(2));
            }

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(conn, statement, results);
        }
        return tuple;
    }

    public class DBWSequenceTuple {

        private int auditId = -1;
        private int archiveId = -1;
        private int eventsId = -1;
        public int getAuditId() {
            return auditId;
        }

        public void setAuditId(int auditId) {
            this.auditId = auditId;
        }

        public int getArchiveId() {
            return archiveId;
        }

        public void setArchiveId(int archiveId) {
            this.archiveId = archiveId;
        }

        public int getEventsId() {
            return eventsId;
        }

        public void setEventsId(int eventsId) {
            this.eventsId = eventsId;
        }

    }

    public String readFromDBTest() throws DBWriterException {

        Statement statement = null;
        Connection conn = DBHelper.getConnection();
        ResultSet results = null;
        String outStr = "";

        StringBuilder sqlBuff = new StringBuilder();
        sqlBuff.append(" SELECT EVENT_ID from dbo.DBW_EVENTS ");

        try {

            log.debug("about to execute sql:[" + sqlBuff.toString() + "]");
            statement = conn.createStatement();

            results = statement.executeQuery(sqlBuff.toString());
            if ( results.next() ) {
                String id = results.getString(1);
                outStr = "sequence number from db is:[" + id + "]";
            } else {
                outStr = "no rows returned";
            }

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            DBHelper.closeResources(conn, statement, results);
        }
        return outStr;
    }
}
