package com.dataagility.dbwriter.dao;

import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.ErrorModel;
import com.dataagility.dbwriter.model.JournalEventsPair;

import java.io.*;
import java.util.List;

/**
 * author: roger lau
 */
public class WriteStringsToFileWriterDAO implements DAO {

    String outputFilename;
    File outputFile;
    OutputStream outputStream;

    public WriteStringsToFileWriterDAO(String outputFilename) {
        this.outputFilename = outputFilename;
        outputFile = new File(outputFilename);
        try {
            //truncate the file
            new FileOutputStream(outputFile).close();
            outputStream = new FileOutputStream(outputFile);
        } catch (FileNotFoundException e) {
            //do nothing b/c it's a test class
        } catch ( IOException ioe) {
            //do nothing
        }
    }

    public void write(String msg) throws DBWriterException {
        try {
            outputStream.write(msg.getBytes());
            outputStream.write('\n');
        } catch (IOException e) {
            //do nothing b/c it's a test class - just print stack trace
            e.printStackTrace();
        }
    }

    @Override
    public void write(JournalEventsPair eventsPair) throws DBWriterException {

    }

    @Override
    public void close() throws DBWriterException {

    }
}