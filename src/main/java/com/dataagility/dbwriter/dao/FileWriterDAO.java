package com.dataagility.dbwriter.dao;

import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.ErrorModel;
import com.dataagility.dbwriter.model.JournalEventsPair;

import java.io.File;
import java.util.List;

/**
 * author: roger lau
 */
public class FileWriterDAO implements DAO {

    String outputFilename;
    File outputFile;

    public FileWriterDAO(String outputFilename) {
        this.outputFilename = outputFilename;
        outputFile = new File(outputFilename);
    }

    @Override
    public void write(JournalEventsPair eventsPair) throws DBWriterException {

    }

    public void close() throws DBWriterException {

    }
}
