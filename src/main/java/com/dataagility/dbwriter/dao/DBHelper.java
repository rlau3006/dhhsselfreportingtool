package com.dataagility.dbwriter.dao;

import com.dataagility.dbwriter.etc.Logger;
import com.dataagility.dbwriter.exception.DBWriterException;

import javax.sql.DataSource;
import java.sql.*;

public class DBHelper {

    private static String jdbcUrl = null;

    private static DataSource datasource = null;

    private static Logger log = new Logger();

    private static Connection connection = null;

    public static Connection getConnection() throws DBWriterException {

        if ( connection != null)
            return connection;

        if (log.isDebugEnabled())
            log.debug("getConnection <entry>");

        connection = getDriverManagerConnection();

        if (log.isDebugEnabled())
            log.debug("getConnection <exit>");
        return connection;
    }

    private static final Connection getDriverManagerConnection() throws DBWriterException {

        Connection conn = null;

        log.info("using driverUrl: " + jdbcUrl);
        try {

            // load the JDBC driver class
            if(jdbcUrl.contains("oracle")) {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } else {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            }

            if (log.isDebugEnabled())
                log.debug("attempting to get connection");

            conn = DriverManager.getConnection(jdbcUrl);

        } catch (Exception e) {
            log.error("failed to get connection");
            throw new DBWriterException(e);
        }

        return conn;
    }

    /*
     * this closes the jdbc resources and provides a clean helper method for all jdbc code.
     * Note that a database connection needs closing, regardless of whether it is
     * a pooled connection (jakarata commons DBCP) or a connection DriverManager-created (for testing)
     */
    public static final void closeResources(Connection conn, Statement statement, ResultSet results) {
        try {
            if (conn != null)
                conn.close();
            if (statement != null)
                statement.close();
            if (results != null)
                results.close();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static final void closeResources(Statement statement, ResultSet results) {
        try {
            if (statement != null)
                statement.close();
            if (results != null)
                results.close();

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static void setJdbcUrl(String jdbcUrl) {
        DBHelper.jdbcUrl = jdbcUrl;
    }
}
