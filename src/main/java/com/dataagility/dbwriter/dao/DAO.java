package com.dataagility.dbwriter.dao;

import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.ErrorModel;
import com.dataagility.dbwriter.model.JournalEventsPair;

import java.util.List;

/**
 * author: roger lau
 */
public interface DAO {
    public void write(JournalEventsPair eventsPair) throws DBWriterException;
//    public void writeJournalEvent(List<JournalEventsPair> model) throws DBWriterException;
    public void close() throws DBWriterException;
}
