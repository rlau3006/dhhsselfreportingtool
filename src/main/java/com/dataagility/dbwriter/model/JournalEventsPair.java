package com.dataagility.dbwriter.model;

/**
 * author: roger lau 2019
 */
public class JournalEventsPair {

    private JournalEvent sentJournalEvent;
    private JournalEvent receivedJournalEvent;

    public JournalEvent getSentJournalEvent() {
        return sentJournalEvent;
    }

    public void setSentJournalEvent(JournalEvent sentJournalEvent) {
        this.sentJournalEvent = sentJournalEvent;
    }

    public JournalEvent getReceivedJournalEvent() {
        return receivedJournalEvent;
    }

    public void setReceivedJournalEvent(JournalEvent receivedJournalEvent) {
        this.receivedJournalEvent = receivedJournalEvent;
    }

    public boolean containsBothJournalEvents() {
        if ( sentJournalEvent != null && sentJournalEvent.hasFoundEndOfJournalMarker() && receivedJournalEvent != null && receivedJournalEvent.hasFoundEndOfJournalMarker())
            return true;
        else
            return false;
    }
}
