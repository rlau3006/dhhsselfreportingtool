package com.dataagility.dbwriter.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * author: roger lau 2019
 */
public class SegmentModel {
    private String segmentName;
    private String messageId;
    private String lineRaw;
    private List<FieldPositionModel> fieldPositionModelList = new ArrayList<FieldPositionModel>();

    public SegmentModel(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getLineRaw() {
        return lineRaw;
    }

    public void setLineRaw(String lineRaw) {
        this.lineRaw = lineRaw + '\r';
    }

    public List<FieldPositionModel> getFieldPositionModelList() {
        return fieldPositionModelList;
    }

    public void setFieldPositionModelList(List<FieldPositionModel> fieldPositionModelList) {
        this.fieldPositionModelList = fieldPositionModelList;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SegmentModel))
            return false;
        SegmentModel segmentModel = (SegmentModel) obj;
        return this.segmentName.compareTo(segmentModel.getSegmentName()) == 0;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.segmentName).toHashCode();
    }
}
