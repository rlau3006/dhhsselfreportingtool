package com.dataagility.dbwriter.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * author: roger lau 2019
 */
public class FieldValueModel {
    private String value;
    private int count;

    public FieldValueModel(String value) {
        this.value = value;
        this.count++;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        this.count++;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FieldValueModel))
            return false;
        FieldValueModel fieldValueModel = (FieldValueModel)obj;
        return this.value.compareTo(fieldValueModel.getValue()) == 0 ? true : false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.value).toHashCode();
    }
}
