package com.dataagility.dbwriter.model;

import com.dataagility.dbwriter.etc.Acknowledgement;
import com.dataagility.dbwriter.etc.EventAction;

/**
 * author: roger lau 2019
 */
public class JournalEvent {

    /**
     * member fields to model columns in the DBW_AUDIT table
     */
    private String auditId;
    private long recordDatetime;   //format YYYYMMDDSSmm.XXX
    private EventAction messageSentOrReceived;
    private String serviceName;
    private String sendingApp;
    private String sendingFacility;
    private String receivingApp;
    private String receivingFacility;
    private String messageDateTime;
    private String messageType;
    private String messageEvent;
    private String messageControlId;
    private String processingId;
    private String versionId;
    private String characterSet;
    private String eventDatetime;
    private String patientId;
    private String patientUr;
    private String PatientGivenName;
    private String patientFamilyName;
    private String patientDob;
    private String patientSex;
    private String patientAddress;
    private String patientClass;
    private String visitId;
    private String admitDatetime;
    private String dischargeDatetime;
    private String orderNumber;
    private String resultStatus;
    private String observationDate;
    private String documentId;
    private String documentOid;
    private String documentSetId;
    private String documentType;
    private String userId;
    private String ihi;
    private String consent;
    private String pcehrExist;
    private long ackResponseSentDatetime;
    private String ackResponseSentCode;
    private String ackResponseSentMessage;
    private long ackResponseReceivedDatetime;
    private String ackResponseReceivedCode;
    private String ackResponseReceivedMessage;

    /**
     * member fields to model columns in the DBW_EVENTS table
     */
    private String eventId;
    private String eventService;
    private String eventNotes;

    /**
     * member fields to model columns in the DBW_ARCHIVE table
     */
    private String archiveId;
    private String logMsg;
    private String errorCode;
    private String errorMessage;

    /**
     * additional fields roger has added
     */
    private HL7Message hl7Message;
    private Acknowledgement acknowledgement;
    private boolean foundEndOfJournalMarker = false;
    private boolean startOfStreamEvent = false;


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getSendingApp() {
        return sendingApp;
    }

    public void setSendingApp(String sendingApp) {
        this.sendingApp = sendingApp;
    }

    public String getSendingFacility() {
        return sendingFacility;
    }

    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }

    public String getReceivingApp() {
        return receivingApp;
    }

    public void setReceivingApp(String receivingApp) {
        this.receivingApp = receivingApp;
    }

    public String getReceivingFacility() {
        return receivingFacility;
    }

    public void setReceivingFacility(String receivingFacility) {
        this.receivingFacility = receivingFacility;
    }

    public String getMessageDateTime() {
        return messageDateTime;
    }

    public void setMessageDateTime(String messageDateTime) {
        this.messageDateTime = messageDateTime;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageEvent() {
        return messageEvent;
    }

    public void setMessageEvent(String messageEvent) {
        this.messageEvent = messageEvent;
    }

    public String getMessageControlId() {
        return messageControlId;
    }

    public void setMessageControlId(String messageControlId) {
        this.messageControlId = messageControlId;
    }

    public String getProcessingId() {
        return processingId;
    }

    public void setProcessingId(String processingId) {
        this.processingId = processingId;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getCharacterSet() {
        return characterSet;
    }

    public void setCharacterSet(String characterSet) {
        this.characterSet = characterSet;
    }

    public String getEventDatetime() {
        return eventDatetime;
    }

    public void setEventDatetime(String eventDatetime) {
        this.eventDatetime = eventDatetime;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientUr() {
        return patientUr;
    }

    public void setPatientUr(String patientUr) {
        this.patientUr = patientUr;
    }

    public String getPatientGivenName() {
        return PatientGivenName;
    }

    public void setPatientGivenName(String patientGivenName) {
        PatientGivenName = patientGivenName;
    }

    public String getPatientFamilyName() {
        return patientFamilyName;
    }

    public void setPatientFamilyName(String patientFamilyName) {
        this.patientFamilyName = patientFamilyName;
    }

    public String getPatientDob() {
        return patientDob;
    }

    public void setPatientDob(String patientDob) {
        this.patientDob = patientDob;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getPatientClass() {
        return patientClass;
    }

    public void setPatientClass(String patientClass) {
        this.patientClass = patientClass;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getAdmitDatetime() {
        return admitDatetime;
    }

    public void setAdmitDatetime(String admitDatetime) {
        this.admitDatetime = admitDatetime;
    }

    public String getDischargeDatetime() {
        return dischargeDatetime;
    }

    public void setDischargeDatetime(String dischargeDatetime) {
        this.dischargeDatetime = dischargeDatetime;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getObservationDate() {
        return observationDate;
    }

    public void setObservationDate(String observationDate) {
        this.observationDate = observationDate;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentOid() {
        return documentOid;
    }

    public void setDocumentOid(String documentOid) {
        this.documentOid = documentOid;
    }

    public String getDocumentSetId() {
        return documentSetId;
    }

    public void setDocumentSetId(String documentSetId) {
        this.documentSetId = documentSetId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIhi() {
        return ihi;
    }

    public void setIhi(String ihi) {
        this.ihi = ihi;
    }

    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public String getPcehrExist() {
        return pcehrExist;
    }

    public void setPcehrExist(String pcehrExist) {
        this.pcehrExist = pcehrExist;
    }

    public long getAckResponseSentDatetime() {
        return ackResponseSentDatetime;
    }

    public void setAckResponseSentDatetime(long ackResponseSentDatetime) {
        this.ackResponseSentDatetime = ackResponseSentDatetime;
    }

    public String getAckResponseSentCode() {
        return ackResponseSentCode;
    }

    public void setAckResponseSentCode(String ackResponseSentCode) {
        this.ackResponseSentCode = ackResponseSentCode;
    }

    public String getAckResponseSentMessage() {
        return ackResponseSentMessage;
    }

    public void setAckResponseSentMessage(String ackResponseSentMessage) {
        this.ackResponseSentMessage = ackResponseSentMessage;
    }

    public long getAckResponseReceivedDatetime() {
        return ackResponseReceivedDatetime;
    }

    public void setAckResponseReceivedDatetime(long ackResponseReceivedDatetime) {
        this.ackResponseReceivedDatetime = ackResponseReceivedDatetime;
    }

    public String getAckResponseReceivedCode() {
        return ackResponseReceivedCode;
    }

    public void setAckResponseReceivedCode(String ackResponseReceivedCode) {
        this.ackResponseReceivedCode = ackResponseReceivedCode;
    }

    public String getAckResponseReceivedMessage() {
        return ackResponseReceivedMessage;
    }

    public void setAckResponseReceivedMessage(String ackResponseReceivedMessage) {
        this.ackResponseReceivedMessage = ackResponseReceivedMessage;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getLogMsg() {
        return logMsg;
    }

    public void setLogMsg(String logMsg) {
        this.logMsg = logMsg;
    }

    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getEventService() {
        return eventService;
    }

    public void setEventService(String eventService) {
        this.eventService = eventService;
    }

    public String getEventNotes() {
        return eventNotes;
    }

    public void setEventNotes(String eventNotes) {
        StringBuilder notesStrBldr = new StringBuilder().append(this.eventNotes).append('\r').append(eventNotes);
        this.eventNotes = notesStrBldr.toString();
    }

    public HL7Message getHl7Message() {
        return hl7Message;
    }

    public void setHl7Message(HL7Message hl7Message) {
        this.hl7Message = hl7Message;
    }

    public EventAction getMessageSentOrReceived() {
        return messageSentOrReceived;
    }

    public void setMessageSentOrReceived(EventAction messageSentOrReceived) {
        this.messageSentOrReceived = messageSentOrReceived;
    }

    public long getRecordDatetime() {
        return recordDatetime;
    }

    public void setRecordDatetime(long recordDatetime) {
        this.recordDatetime = recordDatetime;
    }

    public Acknowledgement getAcknowledgement() {
        return acknowledgement;
    }

    public void setAcknowledgement(Acknowledgement acknowledgement) {
        this.acknowledgement = acknowledgement;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean hasFoundEndOfJournalMarker() {
        return foundEndOfJournalMarker;
    }

    public void setFoundEndOfJournalMarker(boolean foundEndOfJournalMarker) {
        this.foundEndOfJournalMarker = foundEndOfJournalMarker;
    }

    public String getArchiveId() {
        return archiveId;
    }

    public void setArchiveId(String archiveId) {
        this.archiveId = archiveId;
    }

    public boolean isStartOfStreamEvent() {
        return startOfStreamEvent;
    }

    public void setStartOfStreamEvent(boolean startOfStreamEvent) {
        this.startOfStreamEvent = startOfStreamEvent;
    }
}
