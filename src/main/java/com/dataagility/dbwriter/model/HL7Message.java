package com.dataagility.dbwriter.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * author: roger lau 2019
 */
public class HL7Message {
    private String eventType;
    private String messageId;

    private List<SegmentModel> segmentModelList = new ArrayList<SegmentModel>();

    public String getEventType() {
        return eventType;
    }

    public List<SegmentModel> getSegmentModelList() {
        return segmentModelList;
    }

    public HL7Message(String eventType) {
        this.eventType = eventType;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof HL7Message))
            return false;
        HL7Message message = (HL7Message)obj;
        return this.eventType.compareTo(message.getEventType()) == 0 ? true : false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.eventType).toHashCode();
    }
}
