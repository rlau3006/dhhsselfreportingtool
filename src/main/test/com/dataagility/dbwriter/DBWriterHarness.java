package com.dataagility.dbwriter;

import com.dataagility.dbwriter.exception.DBWriterException;

import java.io.IOException;

/**
 * author: roger lau 2019
 */
public class DBWriterHarness {

    public static void main(String[] args) {

        String[] arguments = {
                "-f",
                "C:\\Users\\rlau\\code.area\\ExceptionReportingTool\\src\\main\\resources\\dbwriter.cfg"};
//        String[] arguments = {"-?"};

        try {
            DBWriter.main(arguments);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DBWriterException e) {
            e.printStackTrace();
        }
    }
}
