package com.dataagility.dbwriter.dao;

import com.dataagility.dbwriter.exception.DBWriterException;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DBHelperTest {

    @Test
    public void testgetAConnection() throws DBWriterException {
        assertNotNull(DBHelper.getConnection());
    }
}
