package com.dataagility.dbwriter.dao;

import com.dataagility.dbwriter.etc.Acknowledgement;
import com.dataagility.dbwriter.etc.EventAction;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.model.JournalEvent;
import com.dataagility.dbwriter.model.JournalEventsPair;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class DatabaseWriterDAOTest {

    /*@Test
    public void testWriteSingleRowToDBWEventsTable() throws DBWriterException {
        DatabaseWriterDAO dao = new DatabaseWriterDAO();
        dao.writeToDBTest();
    }*/

    @Test
    public void testWriteJournalEventToDBW_AUDITtable() throws DBWriterException {
        String jdbcUrl = "jdbc:oracle:thin:rla/pwd@localhost:1521/xepdb1";
        DBHelper.setJdbcUrl(jdbcUrl);
        DatabaseWriterDAO dao = new DatabaseWriterDAO();
        JournalEventsPair eventsPair = new JournalEventsPair();

        JournalEvent sentJournalEvent = new JournalEvent();
        sentJournalEvent.setAuditId("88");
        sentJournalEvent.setAcknowledgement(Acknowledgement.ACK);
        sentJournalEvent.setAckResponseReceivedCode("xxx");
        sentJournalEvent.setAckResponseReceivedDatetime(System.currentTimeMillis());
        sentJournalEvent.setAckResponseReceivedMessage("xxx");
        sentJournalEvent.setAckResponseSentCode("xxx");
        sentJournalEvent.setAckResponseSentDatetime(System.currentTimeMillis());
        sentJournalEvent.setAckRespSentMessage("xxx");
        sentJournalEvent.setMessageSentOrReceived(EventAction.SENT);
        sentJournalEvent.setAdmitDatetime("xxx");
        sentJournalEvent.setAuditId("xxx");
        sentJournalEvent.setCharacterSet("xxx");
        sentJournalEvent.setConsent("xxx");
        sentJournalEvent.setDischargeDatetime("xxx");
        sentJournalEvent.setDocumentId("xxx");
        sentJournalEvent.setDocumentOid("xxx");
        sentJournalEvent.setDocumentSetId("xxx");
        sentJournalEvent.setDocumentType("xxx");
        sentJournalEvent.setErrorCode("xxx");
        sentJournalEvent.setErrorMessage("xxx");
        sentJournalEvent.setEventDatetime("xxx");
        sentJournalEvent.setEventId("1"); //this string gets parsed to a long with method Long.parseLong
        sentJournalEvent.setEventNotes("xxx");
        sentJournalEvent.setEventService("xxx");
        sentJournalEvent.setIhi("xxx");
        sentJournalEvent.setLogMsg("xxx");
        sentJournalEvent.setMessageControlId("xxx");
        sentJournalEvent.setMessageDateTime("xxx");
        sentJournalEvent.setMessageEvent("xxx");
        sentJournalEvent.setMessageType("xxx");
        sentJournalEvent.setObservationDate("xxx");
        sentJournalEvent.setOrderNumber("xxx");
        sentJournalEvent.setPatientAddress("xxx");
        sentJournalEvent.setPatientClass("xxx");
        sentJournalEvent.setPatientDob("xxx");
        sentJournalEvent.setPatientFamilyName("xxx");
        sentJournalEvent.setPatientGivenName("xxx");
        sentJournalEvent.setPatientId("xxx");
        sentJournalEvent.setPatientSex("xxx");
        sentJournalEvent.setPatientUr("xxx");
        sentJournalEvent.setPcehrExist("xxx");
        sentJournalEvent.setProcessingId("xxx");
        sentJournalEvent.setReceivingApp("xxx");
        sentJournalEvent.setReceivingFacility("xxx");
        sentJournalEvent.setResultStatus("xxx");
        sentJournalEvent.setSendingApp("xxx");
        sentJournalEvent.setSendingFacility("xxx");
        sentJournalEvent.setServiceName("xxx");
        sentJournalEvent.setRecordDatetime(System.currentTimeMillis());
        sentJournalEvent.setUserId("xxx");
        sentJournalEvent.setVersionId("xxx");
        sentJournalEvent.setVisitId("xxx");

        eventsPair.setSentJournalEvent(sentJournalEvent);
        dao.write(eventsPair);
    }
}
