package com.dataagility.dbwriter.parser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class HSIEToHIPSLogFileParserTest {

    private HSIEToHIPSLogFileParser parser;
    private final String testResourcesDirectoryPath = "C:\\Users\\rlau\\code.area\\ExceptionReportingTool\\src\\main\\resources\\test\\";
    private String testFilename = "testHl7MsgSkipTest.log";

    @Before
    public void setUp() throws Exception {
        parser = new HSIEToHIPSLogFileParser();
        parser.setLogFile(testResourcesDirectoryPath + testFilename);
    }

    @Test
    public void testForInvalidJournalLines() {
        assertEquals(true, parser.isInvalidJournalLine(line10));
        assertEquals(true, parser.isInvalidJournalLine(line16));
        assertEquals(true, parser.isInvalidJournalLine(line17));
        assertEquals(true, parser.isInvalidJournalLine(line18));
        assertEquals(true, parser.isInvalidJournalLine(line21));
        assertEquals(true, parser.isInvalidJournalLine(line23));
        assertEquals(true, parser.isInvalidJournalLine(invalidSegmentNameLine));
        assertEquals(true, parser.isInvalidJournalLine("\n"));
        assertEquals(true, parser.isInvalidJournalLine("\r"));
        assertEquals(true, parser.isInvalidJournalLine("~"));
        assertEquals(true, parser.isInvalidJournalLine("\\"));
    }

    @Test
    public void testForValidJournalLines() {
        assertEquals(false, parser.isInvalidJournalLine(line0));
        assertEquals(false, parser.isInvalidJournalLine(line1));
        assertEquals(false, parser.isInvalidJournalLine(line2));
        assertEquals(false, parser.isInvalidJournalLine(line3));
        assertEquals(false, parser.isInvalidJournalLine(line4));
        assertEquals(false, parser.isInvalidJournalLine(line5));
        assertEquals(false, parser.isInvalidJournalLine(line6));
        assertEquals(false, parser.isInvalidJournalLine(line7));
        assertEquals(false, parser.isInvalidJournalLine(line8));
        assertEquals(false, parser.isInvalidJournalLine(line9));
        assertEquals(false, parser.isInvalidJournalLine(line11));
        assertEquals(false, parser.isInvalidJournalLine(line12));
        assertEquals(false, parser.isInvalidJournalLine(line13));
        assertEquals(false, parser.isInvalidJournalLine(line14));
        assertEquals(false, parser.isInvalidJournalLine(line15));
        assertEquals(false, parser.isInvalidJournalLine(line19));
        assertEquals(false, parser.isInvalidJournalLine(line20));
        assertEquals(false, parser.isInvalidJournalLine(line22));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return null;
    }

    private String line0 = "====== 12:03:2019 - 00:05:11:577  [SENT]";
    private String line1 = "inputMessage:MSH|^~\\&|Unicare|1450|HSIE|1450|20190312000508||ADT^A08|20000000000064288434|P|2.4|||||AUS||en^^ISO 639-1";
    private String line2 = "EVN||20190312000507";
    private String line3 = "PID|||9731465469^^^1450^MR~\"\"^^^AUSHIC^MC~\"\"^^^AUSDVA^DVA||MISALRDFIS^GEORGE^\"\"^^MR^^L||19520309|M|\"\"|4^NOT INDIGENOUS^NHDD-01|PORT PHILLIP PRISON^DOHERTY STREET^LAVERTON^VIC^3028^AUSTRALIA^C||^PRN^PH^\"\"^^^0392154210~^^CP^^^^\"\"|^WPN^PH^^^^\"\"|19^ENGLISH^NHDD-132|M^MARRIED^HL70002|G/O^GREEK ORTHODOX^webPAS|7259384|||\"\"||2205|||3|\"\"|PRI^PRISONER^webPAS|\"\"|N";
    private String line4 = "PV1||I|zPJ^^07A^^N|EAD|7259384|zPJ^^06A||\"\"|LIJ^LI^JIA^^^DR^^webPAS~\"\"^LI^JIA^^^DR^^Provider1~\"\"^LI^JIA^^^DR^^Provider2~\"\"^LI^JIA^^^DR^^Provider3~\"\"^LI^JIA^^^DR^^Provider4~\"\"^LI^JIA^^^DR^^Provider5~\"\"^LI^JIA^^^DR^^Prescriber|DR||||1||||10|7259384|PRN||||||||||||||||||FW^FULL^webPAS||||||20160518213109||||||\"\"^^^LEGACY^VN~\"\"^^^webPAS-ED^VN~\"\"^^^webPAS-SA^VN||\"\"";
    private String line5 = "PV2||||||||20160518213109|20180731052428|0||||||||Y";
    private String line6 = "DG1|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"";
    private String line7 = "GT1|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"";
    private String line8 = "GT1|1||MR G. MISALIS||PORT PHILLIP PRISON^DOHERTY STREET^LAVERTON^VIC^3028|^PRN^PH^^^^0392177210~^^CP^^^^\"\"|^WPN^PH^^^^\"\"||||SELF^SELF^webPAS";
    private String line9 = "IN1|1|\"\"|\"\"|||||||||||||||||||||||||||||||||\"\"";
    private String line10 = "";
    private String line11 = "outputMessage:MSH|^~\\&|Unicare|1450|HSIE|1450|20190312000508||ADT^A08|20000000000064288434|P|2.4|||||AUS||en^^ISO 639-1";
    private String line12 = "EVN||20190312000507";
    private String line13 = "PID|||000973169^^^1450^MR||MISALIS^GEORGE^^^MR^^L||19520309|M||4^NOT INDIGENOUS^NHDD-01|PORT PHILLIP PRISON^DOHERTY STREET^LAVERTON^VIC^3028^AUSTRALIA^C||^PRN^PH^^^^0392177210~^^CP^^^^|^WPN^PH^^^^|19^ENGLISH^NHDD-132|M^MARRIED^HL70002|G/O^GREEK ORTHODOX^webPAS|7259384|||||2205|||3||PRI^PRISONER^webPAS||N";
    private String line14 = "PV1||I|zPJ^^07A^^N|EAD|7259384|zPJ^^06A|||LIJ^LI^JIA^^^DR^^webPAS~^LI^JIA^^^DR^^Provider1~^LI^JIA^^^DR^^Provider2~^LI^JIA^^^DR^^Provider3~^LI^JIA^^^DR^^Provider4~^LI^JIA^^^DR^^Provider5~^LI^JIA^^^DR^^Prescriber|DR||||1||||10|7259384^^^1450|PRN||||||||||||||||||FW^FULL^webPAS||||||20160518213109||||||^^^LEGACY^VN~^^^webPAS-ED^VN~^^^webPAS-SA^VN||";
    private String line15 = "PV2||||||||20160518213109|20180731052428|0||||||||Y";
    private String line16 = "";
    private String line17 = "Using 'AUTHORISED_EMPLOYEE'";
    private String line18 = "";
    private String line19 = "====== 12:03:2019 - 00:05:11:592  [RECEIVED]";
    private String line20 = "AA:MsgControl ID : XHE4yMuPPEKHEyA/9QVA        request was successful";
    private String line21 = "";
    private String line22 = "sendHIPS_ADT : true    Consent :Y";
    private String line23 = "";

    private String invalidSegmentNameLine = "5G1|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"";

}
