package com.dataagility.dbwriter.parser;

import com.dataagility.dbwriter.exception.DBWriterEndOfFileException;
import com.dataagility.dbwriter.exception.DBWriterException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class ProcessPcehrLogFileParserTest {

    private ProcessPcehrLogFileParser parser;
    private final String testResourcesDirectoryPath = "C:\\Users\\rlau\\code.area\\ExceptionReportingTool\\src\\main\\test\\resources\\";
//    private String testFilename = "testProcessPcehrExist_20190521.log";
    private String testFilename = "testProcessPcehrExist_20190521-result-true.log";

    @Before
    public void setUp() throws Exception {
        parser = new ProcessPcehrLogFileParser();
        parser.setLogFile(testResourcesDirectoryPath + testFilename);
    }

    /*@Test
    public void testPropertyNameValuePairRegexValidator() {
//        RegexValidator regexValidator = new RegexValidator("^MessageControlID=.+|^SendingFacility=.+|FoundMRN=.+|DateOfBirth=.+|Error=.+|RepeatedErrorInfo=.+|Result=.+|AcceptedMRN=.+");
        RegexValidator regexValidator = new RegexValidator("^MessageControlID\\=.+");
        assertTrue(regexValidator.isValid(line1));
        int stop = 1;
    }*/

    @Test
    public void testDebugGo() throws DBWriterException, DBWriterEndOfFileException {
        parser.parseFile();
    }

    @Test
    public void testForInvalidJournalLines() {
        assertEquals(true, parser.isInvalidJournalLine(invalidSegmentNameLine));
        assertEquals(true, parser.isInvalidJournalLine("\n"));
        assertEquals(true, parser.isInvalidJournalLine("\r"));
        assertEquals(true, parser.isInvalidJournalLine("~"));
        assertEquals(true, parser.isInvalidJournalLine("\\"));
    }

    @Test
    public void testForValidJournalLines() {
        assertEquals(false, parser.isInvalidJournalLine(line0));
        assertEquals(false, parser.isInvalidJournalLine(line1));
        assertEquals(false, parser.isInvalidJournalLine(line2));
        assertEquals(false, parser.isInvalidJournalLine(line3));
        assertEquals(false, parser.isInvalidJournalLine(line4));
        assertEquals(false, parser.isInvalidJournalLine(line5));
        assertEquals(false, parser.isInvalidJournalLine(line6));
        assertEquals(false, parser.isInvalidJournalLine(line7));
        assertEquals(false, parser.isInvalidJournalLine(line8));
        assertEquals(false, parser.isInvalidJournalLine(line9));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return null;
    }

    private String line0 = "====== 21:05:2019 - 09:30:19:570  [SENT]\n";
    private String line1 = "MessageControlId=Q4521008T5461121\n";
    private String line2 = "SendingFacility=1170\n";
    private String line3 = "FoundMRN=8520924^^^MH^MR~C-U^^^AUSHIC^MC\n";
    private String line4 = "DateOfBirth=19870612000000\n";
    private String line5 = "UserRole=AuthorisedEmployee\n";
    private String line6 = "INFO (InputMessage): MSH|^~\\&|CERNER|1170|HSIE|1170|20190521093012+1000||REF^I12|Q4521008T5461121|D|2.4||||||8859/1\r";
    private String line7 = "RF1|A^Accepted^HL70283|||DS^Discharge Summary^HL70282||2111^MH Dandenong|20190521093014|||E^Event Summary^HL701228\r";
    private String line8 = "PRD|RT^Referred To Provider^HL70286|Lucas^Jennifer Anne|||03 9594 6666^WPN^PH~03 9594 6111^WPN^FX||36690_29786^PERSONNELBADGEID^MH_iPM_ID\r";
    private String line9 = "PID|1|8520924^^^MH^MRN|8520924^^^MH^MR~C-U^^^AUSHIC^MC||Pltest^Four^^^MRS^^L||19870612000000+1000|F|||123 Fake Street^^Clayton^VIC^3168^1100^4~^^^^^1100^BR||~1234 1234 123^PRN^PH||1201|5|7010||C-U|||4|||0|PBS Eligibility Unknown||||N\r";

    private String invalidSegmentNameLine = "5G1|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"|\"\"";

}
