package com.dataagility.dbwriter.parser;

import com.dataagility.dbwriter.exception.DBWriterEndOfFileException;
import com.dataagility.dbwriter.exception.DBWriterException;
import com.dataagility.dbwriter.thread.JournalEventThreadsafeDoubleEndedQueue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class AuditLogFileParserTest {

    private final String testResourcesDirectoryPath = "C:\\Users\\rlau\\code.area\\ExceptionReportingTool\\src\\main\\resources\\test\\";
    private String testFilename = "oneLineTestFile.txt";
    private HSIEToHIPSLogFileParser parser;

    @Before
    public void setUp() throws Exception {
        parser = new HSIEToHIPSLogFileParser();
        parser.setLogFile(testResourcesDirectoryPath + testFilename);
    }

    @Test
    public void testReadLineReturnsLineWithoutCarriageReturnOrNewlineChar() throws DBWriterEndOfFileException, DBWriterException {
        String line = parser.readLineWithEndOfLineCR();
        assertThat(line, containsString("======"));
        assertThat(line, containsString("12:03:2019"));
        assertThat(line, containsString("00:05:11:577"));
        assertThat(line, endsWith("[SENT]"));
        assertThat(line, not(containsString("\n")));
    }

    @Test
    public void testIsSegmentLineByName() {
        String testSegmentLine = "MSH|^~\\&|CERNER|1170|HSIE|1170|20190521144148+1000||REF^I12|Q4524403T5464680|D|2.4||||||8859/1";
        assertTrue(parser.isSegmentByName(testSegmentLine, "MSH"));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return null;
    }
}
